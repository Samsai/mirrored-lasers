FROM debian:buster

RUN apt-get update && apt-get install -y cargo rustc libsdl2-dev libsdl2-mixer-dev libsdl2-image-dev libsdl2-gfx-dev libsdl2-ttf-dev git build-essential
RUN git clone https://github.com/suve/copydeps.git && cd copydeps && make && make install && cd ..

VOLUME /usr/src
WORKDIR /usr/src
