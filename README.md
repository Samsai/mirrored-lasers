# Robot Ricochet (formerly Mirrored Lasers)

You are a sneaky smuggler, who has had an unlucky encounter with the
galactic police bots. Now you must evade capture by destroying
the robots that have come aboard.

Luckily you have your blaster pistol and the cargo hold is full
of mirrors!

## Installing and building

The game is built using the Rust programming language and SDL2.

You can install the Rust compiler and build tools using https://rustup.rs

On Arch Linux the following commands should get you set up:

``` sh
$ sudo pacman -S rustup sdl2 sdl2_gfx sdl2_mixer sdl2_image sdl2_ttf
$ rustup install stable
```

The program is built using ```cargo```:

``` sh
$ cargo build --release

$ ./target/release/mirrored-lasers
```

The repository contains a Dockerfile to set up a Debian-based container for
building release versions of the game. Refer to ```build_release.sh``` if you
want to create release builds as delivered on Itch.io.

## How to play

Your task on each level is to destroy all of the robots to win. To do this you
are armed with a blaster pistol. However, your pistol is a bit broken and the
robots can very quickly see you if you are out in the open.

Luckily the solution to both problems is the same: you can ricochet your shots
or fire through energy fields to ensure a lethal hit. Your shots will alternate
between deadly and harmless based on every ricochet or an energy field it
passes, so plan your shots and make them count. You can also find splitters
which will split your shots into additional shots for maximum mayhem. Just,
watch out to make sure you don't shoot yourself by accident, will you?

To move around you use the WASD keys and to aim and shoot you use the mouse. In
menus the arrow keys are used for moving, Enter is used for selections and
Escape is used to move to previous menus. 

Alternatively you can use gamepad controls where one stick controls movement and
the other controls aiming. The Right Trigger will allow you to shoot, while A
key is used to select things on menus and B is used to go back on menus.

## Credits and attributions

Project idea, sprite work and coding by Samsai
- Code: GPLv3
- Sprites: CC-BY-SA

Graphical work by RoxiTheUnicorn (CC-0, public domain)
- Wall and floor tiles

Menu sounds by broumbroum (CC-BY): https://freesound.org/people/broumbroum/

Music by Tumocs (CC-BY-SA): https://soundcloud.com/tumocs/miami-starsky

Various sound effects (CC-0, public domain)
