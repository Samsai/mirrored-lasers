#!/usr/bin/env bash
set -euo pipefail

mkdir -p dist
mkdir -p dist/libs

cp -rf data dist/data
cargo build --release
cp target/release/mirrored-lasers dist/mirrored-lasers

cp /usr/lib/x86_64-linux-gnu/libSDL2-2.0.so.0 dist/libs
cp /usr/lib/x86_64-linux-gnu/libSDL2_mixer-2.0.so.0 dist/libs
cp /usr/lib/x86_64-linux-gnu/libSDL2_image-2.0.so.0 dist/libs
cp /usr/lib/x86_64-linux-gnu/libSDL2_ttf-2.0.so.0 dist/libs
cp /usr/lib/x86_64-linux-gnu/libsndio.so.7.0 dist/libs
cp /usr/lib/x86_64-linux-gnu/libfluidsynth.so.2 dist/libs
cp /usr/lib/x86_64-linux-gnu/libwebp.so.6 dist/libs
cp /lib/x86_64-linux-gnu/libreadline.so.8 dist/libs
cp /usr/lib/libopusfile.so.0 dist/libs
cp /usr/lib/x86_64-linux-gnu/libopus.so.0 dist/libs
cp /usr/lib/x86_64-linux-gnu/libvorbisfile.so.3 dist/libs
cp /usr/lib/x86_64-linux-gnu/libvorbis.so.0 dist/libs
cp /usr/lib/x86_64-linux-gnu/libvorbisenc.so.2 dist/libs
