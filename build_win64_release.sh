#!/usr/bin/env bash

mkdir -p dist-win64

cp -rf data dist-win64/data
cargo build --release --target x86_64-pc-windows-gnu
cp target/x86_64-pc-windows-gnu/release/robot-ricochet.exe dist-win64/robot-ricochet.exe

cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/*.dll dist-win64/
