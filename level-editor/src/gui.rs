use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::video::Window;

use std::cell::RefCell;

use crate::RenderContext;
use crate::MouseState;

pub trait Widget<T> {
    fn draw(&self, render_context: &mut RenderContext);
    fn handle(&self, event: &Event, mouse_state: &MouseState, gui_state: &mut T);
}

pub struct GuiContext<T> {
    pub state: T,
    widgets: Vec<Box<dyn Widget<T>>>,
}

impl<'a, T> GuiContext<T> {
    pub fn with(state: T) -> GuiContext<T> {
        GuiContext::<T> {
            state,
            widgets: Vec::new(),
        }
    }

    pub fn add_widget<W: 'static + Widget<T>>(&mut self, widget: W) -> &mut Self {
        self.widgets.push(Box::new(widget));

        return self;
    }

    pub fn draw(&self, canvas: &mut RenderContext) {
        for widget in &self.widgets {
            widget.draw(canvas);
        }
    }

    pub fn handle(&mut self, event: &Event, mouse_state: &MouseState) {
        let mut state = &mut self.state;

        for widget in &mut self.widgets {
            widget.handle(event, mouse_state, state);
        }
    }
}

pub struct Button<'a, T> {
    text: String,
    area: Rect,
    callback: &'a Fn(&mut T),
}

impl<T> Button<'_, T> {
    pub fn new<'a>(text: &str, area: Rect, callback: &'a Fn(&mut T)) -> Button<'a, T> {
        Button::<T> {
            text: String::from(text),
            area,
            callback,
        }
    }
}

impl<T> Widget<T> for Button<'_, T> {
    fn draw(&self, render_context: &mut RenderContext) {
        if let Some(texture) = render_context
            .font_manager
            .render(&self.text, Color::RGB(255, 255, 255))
        {
            render_context
                .canvas
                .set_draw_color(Color::RGB(100, 100, 100));
            render_context.canvas.fill_rect(self.area);

            let width = texture.query().width;
            let height = texture.query().height;

            render_context.canvas.copy(
                &texture,
                None,
                Rect::new(self.area.x, self.area.y, width, height),
            );
        }
    }

    fn handle(&self, event: &Event, mouse_state: &MouseState, gui_state: &mut T) {
        if (mouse_state.click) {
            let (x, y) = mouse_state.cursor_coordinate;
           
            if self.area.contains_point((x, y)) {
                (self.callback)(gui_state);
            }
        }
    }
}

pub struct Label<'a, T> {
    pub text: RefCell<String>,
    area: Rect,
    callback: Option<&'a Fn(&Self, &mut T, &Event)>,
}

impl<'a, T> Label<'a, T> {
    pub fn new(text: &str, area: Rect) -> Label<'a, T> {
        Label::<T> {
            text: RefCell::new(String::from(text)),
            area,
            callback: None,
        }
    }
    pub fn new_with_callback(
        text: &str,
        area: Rect,
        callback: &'a Fn(&Self, &mut T, &Event),
    ) -> Label<'a, T> {
        Label::<T> {
            text: RefCell::new(String::from(text)),
            area,
            callback: Some(callback),
        }
    }
}
impl<T> Widget<T> for Label<'_, T> {
    fn draw(&self, render_context: &mut RenderContext) {
        let text = self.text.borrow();

        if text.len() > 0 {
            if let Some(texture) = render_context
                .font_manager
                .render(&text, Color::RGB(255, 255, 255))
            {
                let width = texture.query().width;
                let height = texture.query().height;

                render_context.canvas.copy(
                    &texture,
                    None,
                    Rect::new(self.area.x, self.area.y, width, height),
                );
            }
        }
    }

    fn handle(&self, event: &Event, mouse_state: &MouseState, gui_state: &mut T) {
        if let Some(callback) = self.callback {
            (callback)(self, gui_state, event);
        }
    }
}

pub struct TextBox<'a, T> {
    pub text: RefCell<String>,
    area: Rect,
    callback: Option<&'a Fn(&Self, &mut T, &Event)>,
}

impl<'a, T> TextBox<'a, T> {
    pub fn new(text: &str, area: Rect) -> TextBox<'a, T> {
        TextBox::<T> {
            text: RefCell::new(String::from(text)),
            area,
            callback: None,
        }
    }

    pub fn new_with_callback(
        text: &str,
        area: Rect,
        callback: &'a Fn(&Self, &mut T, &Event),
    ) -> TextBox<'a, T> {
        TextBox::<T> {
            text: RefCell::new(String::from(text)),
            area,
            callback: Some(callback),
        }
    }
}
impl<T> Widget<T> for TextBox<'_, T> {
    fn draw(&self, render_context: &mut RenderContext) {
        render_context
            .canvas
            .set_draw_color(Color::RGB(255, 255, 255));
        render_context.canvas.fill_rect(self.area);

        let text = self.text.borrow();

        if text.len() > 0 {
            if let Some(texture) = render_context
                .font_manager
                .render(&text, Color::RGB(0, 0, 0))
            {
                let width = texture.query().width;
                let height = texture.query().height;

                render_context.canvas.copy(
                    &texture,
                    None,
                    Rect::new(self.area.x, self.area.y, width, height),
                );
            }
        }
    }

    fn handle(&self, event: &Event, mouse_state: &MouseState, gui_state: &mut T) {
        let mut borrowed_text = self.text.borrow_mut();

        match event {
            Event::TextInput { text, .. } => borrowed_text.push_str(text),
            Event::KeyDown {
                keycode: Some(key), ..
            } => match key {
                Keycode::Backspace => {
                    borrowed_text.pop();
                }
                Keycode::Return => borrowed_text.push('\n'),
                _ => (),
            },
            _ => (),
        }

        if let Some(callback) = self.callback {
            (callback)(self, gui_state, event);
        }
    }
}
