extern crate serde;
extern crate serde_json;

use serde::{Deserialize, Serialize};

use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::path::Path;

#[derive(Serialize, Deserialize)]
pub struct Robot {
    pub location: (f32, f32),
    pub path: Vec<(f32, f32)>,
}

#[derive(Serialize, Deserialize)]
pub struct Cargo {
    pub location: (f32, f32),
}

#[derive(Serialize, Deserialize)]
pub struct DynamicObjects {
    pub starting_location: (f32, f32),
    pub robots: Vec<Robot>,
    pub boxes: Vec<Cargo>,
}

pub struct Level {
    pub width: usize,
    pub height: usize,
    pub tiles: Vec<Vec<Tile>>,
    pub objects: Option<DynamicObjects>,
}

impl Level {
    pub fn new(width: usize, height: usize) -> Level {
        let mut tiles = Vec::new();

        for y in 0..height {
            let mut line = Vec::new();
            for x in 0..width {
                line.push(Tile::Void);
            }

            tiles.push(line);
        }

        let objects = DynamicObjects {
            starting_location: (100.0, 100.0),
            robots: Vec::new(),
            boxes: Vec::new(),
        };

        Level {
            width,
            height,
            tiles,
            objects: Some(objects),
        }
    }

    pub fn load(path: &Path) -> Result<Level, std::io::Error> {
        let mut level = Self::load_level_tiles(&path.join("map.txt"))?;

        level.objects = Some(Self::load_level_objects(&path.join("metadata.json"))?);

        return Ok(level);
    }

    fn load_level_objects(path: &Path) -> Result<DynamicObjects, std::io::Error> {
        let mut level_object_string = String::new();

        let mut file = File::open(path)?;
        file.read_to_string(&mut level_object_string);

        let level_objects: DynamicObjects = serde_json::from_str(&level_object_string).unwrap();

        Ok(level_objects)
    }

    fn load_level_tiles<P: AsRef<Path> + std::fmt::Debug>(
        path: &P,
    ) -> Result<Level, std::io::Error> {
        println!("Trying to load level: {:?}", path);
        println!("In folder {:?}", std::env::current_dir());
        println!("Overall: {:?}", std::env::current_dir().unwrap().join(path));

        let mut level_string = String::new();

        let mut file = File::open(path)?;
        file.read_to_string(&mut level_string);

        let lines: Vec<&str> = level_string.lines().collect();

        let mut it = lines[0].split_whitespace();
        let width = it
            .next()
            .map(|s| s.trim().parse::<usize>().expect("Blarg"))
            .expect("blorg");
        let height = it
            .next()
            .map(|s| s.trim().parse::<usize>().expect("Blarg"))
            .expect("blorg");

        println!("Level size: {} * {}", width, height);

        let mut level = Level::new(width, height);

        let mut x = 0;
        let mut y = 0;

        for i in 1..height + 1 {
            for c in lines[i].chars() {
                match c {
                    'X' => level.tiles[y][x] = Tile::Void,
                    ' ' => level.tiles[y][x] = Tile::Floor,
                    'O' => level.tiles[y][x] = Tile::Mirror,
                    '=' => level.tiles[y][x] = Tile::EnergyField,
                    '$' => level.tiles[y][x] = Tile::Splitter,
                    _ => level.tiles[y][x] = Tile::Wall,
                }
                x += 1;
            }
            x = 0;
            y += 1;
        }

        Ok(level)
    }

    pub fn save(&self, path: &Path) -> Result<(), std::io::Error> {
        if !path.exists() {
            std::fs::create_dir_all(path)?;
        }

        self.save_level_objects(&path.join("metadata.json"))?;
        self.save_level_tiles(&path.join("map.txt"))?;

        return Ok(());
    }

    fn save_level_objects(&self, path: &Path) -> Result<(), std::io::Error> {
        if let Some(objects) = &self.objects {
            let mut file = File::create(path)?;

            let string = serde_json::to_string(objects).unwrap();

            write!(file, "{}", string);
        }

        return Ok(());
    }

    fn save_level_tiles<P: AsRef<Path> + std::fmt::Debug>(&self, path: &P) -> Result<(), std::io::Error> {
        println!("Trying to save level: {:?}", path);
        println!("In folder {:?}", std::env::current_dir());
        println!("Overall: {:?}", std::env::current_dir().unwrap().join(path));

        let mut file = File::create(path)?;

        write!(file, "{} {}\n", self.width, self.height);

        for y in 0..self.height {
            for x in 0..self.width {
                let c = match self.tiles[y][x] {
                    Tile::Void => 'X',
                    Tile::Floor => ' ',
                    Tile::Mirror => 'O',
                    Tile::EnergyField => '=',
                    Tile::Splitter => '$',
                    _ => '#',
                };

                write!(file, "{}", c);
            }
            write!(file, "\n");
        }

        return Ok(());
    }
}

pub enum Tile {
    Void,
    Wall,
    Floor,
    Mirror,
    EnergyField,
    Splitter,
}
