
use sdl2::rect::Rect;
use sdl2::rect::Point;

use std::cell::RefCell;
use std::cell::Cell;
use std::rc::Rc;

use crate::gui::*;
use crate::*;

pub struct LevelView<'a, T> {
    area: Rect,
    level: Rc<RefCell<Level>>,
    callback: &'a Fn(&Self, &mut T, &Event, &MouseState),
    view_robot_path: Cell<Option<usize>>,
    placed_node: Cell<bool>,
    next_path_segment: Cell<Option<(i32, i32)>>,
}

impl<'a, T> LevelView<'a, T> {
    pub fn new(area: Rect, level: Rc<RefCell<Level>>) -> LevelView<'a, GuiState> {
        return LevelView::<GuiState> {
            area,
            level,
            callback: &level_view_handler,
            view_robot_path: Cell::new(None),
            placed_node: Cell::new(false),
            next_path_segment: Cell::new(None),
        }
    }

    fn to_internal_resolution(&self, x: i32, y: i32) -> (i32, i32) {
        let scale_x = 1280.0 / (self.area.width() as f32);
        let scale_y = 720.0 / (self.area.height() as f32);

        let rel_x = (((x - self.area.x()) as f32) * scale_x) as i32;
        let rel_y = (((y - self.area.y()) as f32) * scale_y) as i32;

        return (rel_x, rel_y);
    }

    fn to_external_resolution(&self, internal_x: i32, internal_y: i32) -> (i32, i32) {
        let scale_x = 1280.0 / (self.area.width() as f32);
        let scale_y = 720.0 / (self.area.height() as f32);

        let x = (((internal_x + self.area.x()) as f32) / scale_x) as i32;
        let y = (((internal_y + self.area.y()) as f32) / scale_y) as i32;

        return (x, y);
    }
}

fn level_view_handler(
    level_view: &LevelView<GuiState>,
    state: &mut GuiState,
    event: &Event,
    mouse_state: &MouseState
) {
    let mut level = level_view.level.borrow_mut();

    let (mouse_x, mouse_y) = mouse_state.cursor_coordinate;

    if level_view.area.contains_point((mouse_x, mouse_y)) {
        let mut modified = true;

        let (rel_x, rel_y) = level_view.to_internal_resolution(mouse_x, mouse_y);

        let tile_x = (rel_x as usize) / 35;
        let tile_y = (rel_y as usize) / 35;

        match &state.mode {
            Mode::Normal => {
                if let Some(objects) = &level.objects {
                    let mut clicked_on_robot = false;

                    for index in 0..objects.robots.len() {
                        let robot = &objects.robots[index];
                        let (x, y) = robot.location;
                        let rect = Rect::from_center((x as i32, y as i32), 32, 32);

                        if mouse_state.click && rect.contains_point((rel_x, rel_y)) {
                            println!("Selected robot {}", index);
                            state.mode = Mode::RobotPathMode(index);
                            level_view.view_robot_path.replace(Some(index));
                            level_view.placed_node.replace(false);
                            clicked_on_robot = true;

                            break;
                        }
                    }

                    if !clicked_on_robot {
                        level_view.view_robot_path.replace(None);
                    }

                    modified = false;
                }
            },
            Mode::RobotPathMode(index) => {
                if let Some(objects) = &mut level.objects {
                    if mouse_state.click {
                        let mut robot = &mut objects.robots[*index];

                        robot.path.clear();
                        robot.path.push((rel_x as f32, rel_y as f32));
                        level_view.placed_node.replace(true);

                        state.mode = Mode::RobotCreatePathMode(*index);
                    } else {
                        level_view.next_path_segment.replace(Some((rel_x, rel_y)));
                    }
                }
            },
            Mode::RobotCreatePathMode(index) => {
                if let Some(objects) = &mut level.objects {
                    if mouse_state.click {
                        let mut robot = &mut objects.robots[*index];
                        robot.path.push((rel_x as f32, rel_y as f32));
                    } else {
                        level_view.next_path_segment.replace(Some((rel_x, rel_y)));
                    }
                }
            },
            Mode::DeleteMode => {
                if let Some(objects) = &mut level.objects {
                    let mut deleted_something = false;

                    if mouse_state.mouse_down {
                        objects.robots.retain(|r| {
                            let (x, y) = r.location;
                            let rect = Rect::new(x as i32, y as i32, 32, 32);

                            let should_be_kept = !rect.contains_point((rel_x, rel_y));

                            if !should_be_kept { deleted_something = true; }

                            should_be_kept
                        });

                        objects.boxes.retain(|b| {
                            let (x, y) = b.location;
                            let rect = Rect::new(x as i32, y as i32, 32, 32);

                            let should_be_kept = !rect.contains_point((rel_x, rel_y));

                            if !should_be_kept { deleted_something = true; }

                            should_be_kept
                        });
                    }

                }
            },
            Mode::PlacementMode(selection) => {
                if mouse_state.mouse_down {
                    if tile_y < level.tiles.len() && tile_x < level.tiles[tile_y].len() {
                        match selection {
                            Selection::None => { modified = false; },
                            Selection::TileVoid => { level.tiles[tile_y][tile_x] = Tile::Void; }
                            Selection::TileWall => { level.tiles[tile_y][tile_x] = Tile::Wall; },
                            Selection::TileFloor => { level.tiles[tile_y][tile_x] = Tile::Floor; }
                            Selection::TileMirror => { level.tiles[tile_y][tile_x] = Tile::Mirror; }
                            Selection::TileEnergy => { level.tiles[tile_y][tile_x] = Tile::EnergyField; }
                            Selection::TileSplitter => { level.tiles[tile_y][tile_x] = Tile::Splitter; }
                            Selection::EntityPlayer => {
                                if let Some(objects) = &mut level.objects {
                                    objects.starting_location = (rel_x as f32, rel_y as f32);
                                }
                            },
                            Selection::EntityBoxCreate => {
                                if let Some(objects) = &mut level.objects {
                                    objects.boxes.push(Cargo { location: (rel_x as f32, rel_y as f32) });

                                    state.mode = Mode::PlacementMode(Selection::EntityBoxSelected(objects.boxes.len() - 1));
                                }
                            },
                            Selection::EntityBoxSelected(index) => {
                                if let Some(objects) = &mut level.objects {
                                    objects.boxes[*index].location = (rel_x as f32, rel_y as f32);
                                }
                            },
                            Selection::EntityRobotCreate => {
                                if let Some(objects) = &mut level.objects {
                                    objects.robots.push(Robot { location: (rel_x as f32, rel_y as f32), path: vec![] });

                                    state.mode = Mode::PlacementMode(Selection::EntityRobotSelected(objects.robots.len() - 1));
                                }
                            },
                            Selection::EntityRobotSelected(index) => {
                                if let Some(objects) = &mut level.objects {
                                    objects.robots[*index].location = (rel_x as f32, rel_y as f32);
                                }
                            }
                            _ => (),
                        }
                    }
                }
            },
            _ => (),
        }

        if (modified) {
            state.modified_status = modified;
        }
    }
}

impl<'a, T> Widget<T> for LevelView<'a, T> {
    fn draw(&self, render_context: &mut RenderContext) {
        let texture_creator = &render_context.canvas.texture_creator();

        let mut level_texture = texture_creator
            .create_texture_target(texture_creator.default_pixel_format(), 1280, 720)
            .unwrap();

        let robot_texture = render_context.texture_manager.get("enemy").unwrap();
        let player_texture = render_context.texture_manager.get("player").unwrap();
        let box_texture = render_context.texture_manager.get("box").unwrap();

        let level = self.level.borrow();

        render_context
            .canvas
            .with_texture_canvas(&mut level_texture, |texture_canvas| {
                texture_canvas.clear();

                for y in 0..level.height {
                    for x in 0..level.width {
                        match level.tiles[y][x] {
                            Tile::Void => {
                                texture_canvas.set_draw_color(Color::RGB(0, 0, 0));
                            },
                            Tile::Floor => {
                                texture_canvas.set_draw_color(Color::RGB(255, 255, 255))
                            }
                            Tile::Wall => {
                                texture_canvas.set_draw_color(Color::RGB(100, 100, 100))
                            }
                            Tile::Mirror => {
                                texture_canvas.set_draw_color(Color::RGB(0, 0, 255))
                            }
                            Tile::EnergyField => {
                                texture_canvas.set_draw_color(Color::RGB(200, 0, 0))
                            }
                            Tile::Splitter => {
                                texture_canvas.set_draw_color(Color::RGB(0, 200, 0))
                            }
                        }

                        texture_canvas.fill_rect(Rect::new(
                            x as i32 * 35,
                            y as i32 * 35,
                            35,
                            35,
                        ));
                    }
                }

                if let Some(objects) = &level.objects {
                    let (start_x, start_y) = objects.starting_location;

                    for robot in &objects.robots {
                        let (x, y) = robot.location;

                        texture_canvas.copy(&robot_texture, Rect::new(0, 0, 32, 32), Rect::from_center((x as i32, y as i32), 32, 32));
                    }

                    for container in &objects.boxes {
                        let (x, y) = container.location;

                        texture_canvas.copy(&box_texture, None, Rect::from_center((x as i32, y as i32), 32, 32));
                    }

                    texture_canvas.copy(&player_texture, Rect::new(0, 0, 32, 32), Rect::from_center((start_x as i32, start_y as i32), 32, 32));
                }
            }).unwrap();

        render_context
            .canvas
            .copy(&level_texture, None, self.area)
            .unwrap();


        if let Some(objects) = &level.objects {
            if let Some(index) = self.view_robot_path.get() {
                if index < objects.robots.len() {
                    let robot = &objects.robots[index];
                    let (start_x, start_y) = &robot.location;

                    let (mut prev_x, mut prev_y) = self.to_external_resolution(*start_x as i32, *start_y as i32);
                    robot.path
                            .iter()
                            .for_each(|(x, y)| {
                                let (true_x, true_y) = self.to_external_resolution(*x as i32, *y as i32);
                                render_context.canvas.set_draw_color(Color::RGB(0, 255, 0));
                                render_context.canvas.fill_rect(Rect::from_center((true_x, true_y), 10, 10));
                                render_context.canvas.set_draw_color(Color::RGB(0, 255, 40));
                                render_context.canvas.draw_line((prev_x, prev_y), (true_x, true_y));

                                prev_x = true_x;
                                prev_y = true_y;
                            });

                    if let Some((next_x, next_y)) = self.next_path_segment.get() {
                        let (nx, ny) = self.to_external_resolution(next_x, next_y);

                        render_context.canvas.set_draw_color(Color::RGB(200, 200, 200));

                        if (self.placed_node.get()) {
                            render_context.canvas.draw_line((prev_x, prev_y), (nx, ny));
                        } else {
                            let (start_x_ex, start_y_ex) = self.to_external_resolution(*start_x as i32, *start_y as i32);
                            render_context.canvas.draw_line((start_x_ex, start_y_ex), (nx, ny));
                        }
                    }
                }
            }
        }
    }

    fn handle(&self, event: &Event, mouse_state: &MouseState, gui_state: &mut T) {
        (self.callback)(&self, gui_state, event, mouse_state);
    }
}
