extern crate sdl2;

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::render::Texture;
use sdl2::render::TextureCreator;
use sdl2::video::Window;

use std::path::Path;
use std::path::PathBuf;
use std::rc::Rc;
use std::cell::RefCell;
use std::env;

mod level;
use level::*;

mod gui;
use gui::*;

mod level_view;
use level_view::LevelView;

mod texture_manager;
use texture_manager::TextureManager;

mod font_manager;
use font_manager::FontManager;

pub struct RenderContext<'a> {
    canvas: Canvas<Window>,
    font_manager: FontManager<'a>,
    texture_manager: TextureManager<'a>,
}

pub enum Selection {
    None,
    TileVoid,
    TileFloor,
    TileWall,
    TileMirror,
    TileEnergy,
    TileSplitter,
    EntityPlayer,
    EntityBoxCreate,
    EntityBoxSelected(usize),
    EntityRobotCreate,
    EntityRobotSelected(usize)
}

pub enum Mode {
    Normal,
    PlacementMode(Selection),
    DeleteMode,
    RobotPathMode(usize),
    RobotCreatePathMode(usize)
}

pub struct GuiState {
    path: PathBuf,
    level: Rc<RefCell<Level>>,
    modified_status: bool,
    mode: Mode,
}

#[derive(Debug)]
pub struct MouseState {
    cursor_coordinate: (i32, i32),
    mouse_down: bool,
    click: bool,
}

impl MouseState {
    fn new() -> MouseState {
        MouseState {
            cursor_coordinate: (0, 0),
            mouse_down: false,
            click: false,
        }
    }
}

fn main() {
    if env::args().count() < 2 {
        panic!("Error: no path given");
    }

    let args: Vec<String> = env::args().skip(1).collect();

    let mut level_path = args.iter().fold(String::new(), |a, s| format!("{} {}", a, s));
    level_path.remove(0);

    println!("Level path is: {}", &level_path);

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    video_subsystem
        .text_input()
        .set_rect(Rect::new(0, 0, 200, 50));
    video_subsystem.text_input().start();

    let ttf = sdl2::ttf::init().unwrap();

    let window = video_subsystem
        .window("mirrored-lasers level editor", 1280, 720)
        .position_centered()
        .resizable()
        .build()
        .unwrap();

    let mut canvas: Canvas<Window> = window
        .into_canvas()
        .accelerated()
        .present_vsync()
        .build()
        .unwrap();

    let mut texture_creator = canvas.texture_creator();

    let mut texture_manager = TextureManager::with(&texture_creator);
    texture_manager.load(
        String::from("player"),
        Path::new("./data/sprite/protagonist.png"),
    );
    texture_manager.load(
        String::from("projectile"),
        Path::new("./data/sprite/projectile.png"),
    );

    texture_manager.load(
        String::from("enemy"),
        Path::new("./data/sprite/robot.png"),
    );

    texture_manager.load(String::from("box"), Path::new("./data/sprite/box.png"));

    let mut font_manager = FontManager::with(&ttf, &texture_creator);
    font_manager.load_font(Path::new("./data/font/FreeMono.ttf"), 16);

    let mut render_context = RenderContext {
        canvas: canvas,
        font_manager: font_manager,
        texture_manager,
    };

    let mut level_texture = texture_creator
        .create_texture_target(texture_creator.default_pixel_format(), 1280, 720)
        .unwrap();

    let mut mouse = MouseState::new();

    render_context
        .canvas
        .set_draw_color(Color::RGB(0, 255, 255));
    render_context.canvas.clear();
    render_context.canvas.present();

    let level = Rc::new(RefCell::new(
        Level::load(Path::new(&level_path)).unwrap_or(Level::new(35, 20))
    ));

    let mut event_pump = sdl_context.event_pump().unwrap();

    let mut gui_state = GuiState {
        path: PathBuf::from(level_path),
        level: level.clone(),
        modified_status: false,
        mode: Mode::Normal
    };

    let mut gui_context = GuiContext::with(gui_state);

    gui_context
        .add_widget(LevelView::<GuiState>::new(Rect::new(20, 20, 900, 505), level.clone()))
        .add_widget(Button::new("Save", Rect::new(200, 0, 40, 20),
                                &|state: &mut GuiState| {
                                    let level = state.level.borrow();

                                    level.save(&state.path).expect("Failed!");

                                    state.modified_status = false;
                                }))
        .add_widget(Label::new_with_callback("Saved", Rect::new(800, 0, 20, 60),
                                             &|s: &Label<GuiState>, state: &mut GuiState, event: &Event| {
                                                 let mut text = s.text.borrow_mut();
                                                 if (state.modified_status) {
                                                     text.clear();
                                                     text.push_str("Modified");
                                                 } else {
                                                     text.clear();
                                                     text.push_str("Saved");
                                                 }
                                            }))
        .add_widget(Label::new_with_callback("Normal", Rect::new(0, 0, 20, 60),
                                             &|s: &Label<GuiState>, state: &mut GuiState, event: &Event| {
                                                 let mut text = s.text.borrow_mut();
                                                 text.clear();

                                                 match &state.mode {
                                                     Mode::Normal => text.push_str("Normal"),
                                                     Mode::PlacementMode(s) => text.push_str("Placement"),
                                                     Mode::DeleteMode => text.push_str("Delete"),
                                                     Mode::RobotPathMode(s) => text.push_str("Show path"),
                                                     Mode::RobotCreatePathMode(s) => text.push_str("Create path"),
                                                     _ => (),
                                                 }
                                            }));

    gui_context
        .add_widget(Button::new("Void", Rect::new(20, 550, 40, 20), &|state: &mut GuiState| { state.mode = Mode::PlacementMode(Selection::TileVoid); }))
        .add_widget(Button::new("Floor", Rect::new(80, 550, 40, 20), &|state: &mut GuiState| { state.mode = Mode::PlacementMode(Selection::TileFloor); }))
        .add_widget(Button::new("Wall", Rect::new(140, 550, 40, 20), &|state: &mut GuiState| { state.mode = Mode::PlacementMode(Selection::TileWall); }))
        .add_widget(Button::new("Mirror", Rect::new(200, 550, 40, 20), &|state: &mut GuiState| { state.mode = Mode::PlacementMode(Selection::TileMirror); }))
        .add_widget(Button::new("Energy", Rect::new(260, 550, 40, 20), &|state: &mut GuiState| { state.mode = Mode::PlacementMode(Selection::TileEnergy); }))
        .add_widget(Button::new("Splitter", Rect::new(320, 550, 40, 20), &|state: &mut GuiState| { state.mode = Mode::PlacementMode(Selection::TileSplitter); }))
        .add_widget(Button::new("Player", Rect::new(900, 20, 40, 20), &|state: &mut GuiState| { state.mode = Mode::PlacementMode(Selection::EntityPlayer); }))
        .add_widget(Button::new("Box", Rect::new(900, 45, 40, 20), &|state: &mut GuiState| { state.mode = Mode::PlacementMode(Selection::EntityBoxCreate); }))
        .add_widget(Button::new("Robot", Rect::new(900, 70, 40, 20), &|state: &mut GuiState| { state.mode = Mode::PlacementMode(Selection::EntityRobotCreate); }))
        .add_widget(Button::new("Delete", Rect::new(900, 100, 40, 20), &|state: &mut GuiState| { state.mode = Mode::DeleteMode; }))
        ;

    'running: loop {
        mouse.click = false;
       
        render_context.canvas.set_draw_color(Color::RGB(0, 0, 0));
        render_context.canvas.clear();

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => break 'running,
                Event::KeyDown { keycode: Some(key), .. } => {
                    match key {
                        Keycode::Escape => gui_context.state.mode = Mode::Normal,
                        _ => (),
                    }
                }
                Event::MouseButtonUp { x, y, .. } => {
                    mouse.mouse_down = false;
                    mouse.cursor_coordinate = (x, y);
                },
                Event::MouseButtonDown { x, y, ..} => {
                    mouse.mouse_down = true;
                    mouse.cursor_coordinate = (x, y);
                    mouse.click = true;
                },
                Event::MouseMotion { x, y, .. } => {
                    mouse.cursor_coordinate = (x, y);
                },
                _ => {}
            }

            gui_context.handle(&event, &mouse);
        }

        gui_context.draw(&mut render_context);

        render_context.canvas.present();
    }
}
