#!/usr/bin/env bash
set -euo pipefail

podman run -it --rm -v $(pwd):/usr/src robot-ricochet-build
