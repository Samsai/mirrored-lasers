#!/usr/bin/env bash

cd ./level-editor && cargo build

cd ..

./level-editor/target/debug/level-editor $1
