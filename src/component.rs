use specs::Entity;
use specs::{Component, VecStorage};

use sdl2::rect::Rect;

use std::collections::HashSet;

use crate::gameplay::level::Tile;

#[derive(Component, Clone)]
#[storage(VecStorage)]
pub struct Position {
    pub x: f32,
    pub y: f32,
}

impl Position {
    pub fn to_tile_coordinate(&self) -> (usize, usize) {
        ((self.x / 35.0) as usize, (self.y / 35.0) as usize)
    }
}

pub type CollisionMask = HashSet<CollisionLayer>;

#[derive(Clone, PartialEq, Eq, Hash)]
pub enum CollisionLayer {
    Player,
    Enemy,
    Projectile,
    Dead,
}

#[derive(Component, Clone)]
#[storage(VecStorage)]
pub struct Collideable {
    pub width: f32,
    pub height: f32,
    pub layer: CollisionLayer,
    pub mask: CollisionMask,
}

#[derive(Component, Clone)]
#[storage(VecStorage)]
pub struct Collision {
    pub collider: Entity,
}

#[derive(Component, Clone)]
#[storage(VecStorage)]
pub struct WallCollision {
    pub tile: Tile,
    pub vertical: bool,
    pub x: usize,
    pub y: usize,
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct Velocity {
    pub x: f32,
    pub y: f32,
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct Projectile {
    pub timer: f32,
    pub num_splits: u8,
    pub out_of_phase: bool,
    pub entered_phase_field: bool,
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct PlayerControlled;

#[derive(Component)]
#[storage(VecStorage)]
pub struct Enemy {
    pub path: Vec<(f32, f32)>,
    pub target: usize,
    pub aim: f32,
    pub hit: bool,
    pub dead: bool,
    pub death_timer: f32,
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct Crate {
    pub hit: bool,
    pub dead: bool,
    pub death_timer: f32,
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct LineOfSight {
    pub intersects: bool,
    pub start: (f32, f32),
    pub target: (f32, f32),
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct FireRateLimited {
    pub rate: f32,
    pub timer: f32,
}

#[derive(PartialEq, Eq, Hash, Clone)]
pub enum SpriteEffect {
    ColourMod(u8, u8, u8),
    AlphaMod(u8),
}

pub type SpriteEffects = HashSet<SpriteEffect>;

#[derive(Component)]
#[storage(VecStorage)]
pub struct Sprite {
    pub name: String,
    pub animation: Animation,
    pub timer: f32,
    pub rotation: f64,
    pub flipped: bool,
    pub effects: SpriteEffects,
}

pub struct Animation {
    pub frames: &'static [Frame],
    pub index: usize,
    pub direction: Direction,
    pub rate: f32,
    pub animation_transition: Option<&'static [Frame]>,
}

impl Animation {
    pub fn new(frames: &'static [Frame], rate: f32) -> Animation {
        Animation {
            frames,
            index: 0,
            direction: Direction::Right,
            rate,
            animation_transition: None,
        }
    }

    pub fn reset(&mut self) {
        self.index = 0;
        self.direction = Direction::Right;
    }
}

pub enum Direction {
    Left,
    Right,
}

pub struct Frame {
    pub x: i32,
    pub y: i32,
    pub w: u32,
    pub h: u32,
}

impl std::convert::Into<Option<Rect>> for &Frame {
    fn into(self) -> Option<Rect> {
        Some(Rect::new(self.x, self.y, self.w, self.h))
    }
}

pub const fn frame(x: i32, y: i32, w: u32, h: u32) -> Frame {
    Frame { x, y, w, h }
}
