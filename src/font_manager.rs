use sdl2::ttf::Font;
use sdl2::ttf::FontStyle;
use sdl2::ttf::Sdl2TtfContext;

use sdl2::render::Texture;
use sdl2::render::TextureCreator;
use sdl2::video::WindowContext;

use sdl2::pixels::Color;

use std::path::Path;

pub struct FontManager<'s> {
    font: Option<Font<'s, 's>>,
    tc: &'s TextureCreator<WindowContext>,
    ttf: &'s Sdl2TtfContext,
}

impl<'s> FontManager<'s> {
    pub fn with(ttf: &'s Sdl2TtfContext, tc: &'s TextureCreator<WindowContext>) -> FontManager<'s> {
        FontManager {
            font: None,
            tc,
            ttf,
        }
    }

    pub fn load_font(&mut self, path: &Path, size: u16) -> Result<(), String> {
        let mut font = self.ttf.load_font(path, size)?;

        font.set_style(FontStyle::BOLD);

        self.font = Some(font);

        Ok(())
    }

    pub fn render(&mut self, text: &str, color: Color) -> Option<Texture> {
        if let Some(font) = &mut self.font {
            let surface = font.render(text).blended(color).unwrap();
            let texture = self.tc.create_texture_from_surface(surface).unwrap();

            return Some(texture);
        }

        None
    }
}
