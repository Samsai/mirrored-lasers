use specs::Builder;
use specs::World;
use specs::WorldExt;

use sdl2::rect::Rect;

use std::collections::HashSet;

use crate::component::*;

pub const player_idle_frames: &[Frame] = &[frame(0, 0, 32, 32)];
pub const player_walking_frames: &[Frame] = &[frame(32, 0, 32, 32), frame(64, 0, 32, 32)];
pub const player_firing_frames: &[Frame] = &[frame(32 * 3, 0, 32, 32), frame(32 * 4, 0, 32, 32)];
pub const player_death_frames: &[Frame] = &[frame(32 * 5, 0, 32, 32), frame(32 * 6, 0, 32, 32)];

pub fn create_player(world: &mut World, x: f32, y: f32) {
    let mut animation = Animation::new(player_idle_frames, 0.1);
    animation.animation_transition = Some(player_idle_frames);

    let mut collision_mask = HashSet::new();
    collision_mask.insert(CollisionLayer::Projectile);

    world
        .create_entity()
        .with(PlayerControlled)
        .with(Sprite {
            name: String::from("sprite"),
            animation,
            timer: 0.0,
            rotation: 0.0,
            flipped: true,
            effects: SpriteEffects::new(),
        })
        .with(Position { x, y })
        .with(Collideable {
            width: 33.0,
            height: 23.0,
            layer: CollisionLayer::Player,
            mask: collision_mask,
        })
        .with(Velocity { x: 0.0, y: 0.0 })
        .with(FireRateLimited {
            rate: 1.0,
            timer: 0.0,
        })
        .build();
}

pub const cargo_frame: &[Frame] = &[frame(0, 0, 32, 32)];

pub fn create_cargo(world: &mut World, x: f32, y: f32) {
    let mut collision_mask = HashSet::new();
    collision_mask.insert(CollisionLayer::Projectile);
    collision_mask.insert(CollisionLayer::Player);
    collision_mask.insert(CollisionLayer::Enemy);

    world
        .create_entity()
        .with(Sprite {
            name: String::from("box"),
            animation: Animation::new(cargo_frame, 0.0),
            timer: 0.0,
            rotation: 0.0,
            flipped: false,
            effects: SpriteEffects::new(),
        })
        .with(Position { x, y })
        .with(Collideable {
            width: 32.0,
            height: 32.0,
            layer: CollisionLayer::Enemy,
            mask: collision_mask,
        })
        .with(Crate {
            hit: false,
            dead: false,
            death_timer: 1.0,
        })
        .build();
}

pub const robot_frames: &[Frame] = &[
    frame(0, 0, 32, 32),
    frame(32 * 1, 0, 32, 32),
    frame(32 * 2, 0, 32, 32),
];
pub const robot_death_frames: &[Frame] = &[
    frame(32 * 3, 0, 32, 32),
    frame(32 * 4, 0, 32, 32),
    frame(32 * 5, 0, 32, 32),
    frame(32 * 6, 0, 32, 32),
    frame(32 * 7, 0, 32, 32),
    frame(32 * 8, 0, 32, 32),
];

pub fn create_robot(world: &mut World, x: f32, y: f32, path: Vec<(f32, f32)>) {
    let mut collision_mask = HashSet::new();
    collision_mask.insert(CollisionLayer::Projectile);
    collision_mask.insert(CollisionLayer::Player);
    collision_mask.insert(CollisionLayer::Enemy);

    world
        .create_entity()
        .with(Enemy {
            path,
            target: 0,
            aim: 0.0,
            hit: false,
            dead: false,
            death_timer: 2.0,
        })
        .with(Sprite {
            name: String::from("enemy"),
            animation: Animation::new(robot_frames, 0.3),
            timer: 0.0,
            rotation: 0.0,
            flipped: false,
            effects: SpriteEffects::new(),
        })
        .with(Position { x, y })
        .with(Collideable {
            width: 32.0,
            height: 32.0,
            layer: CollisionLayer::Enemy,
            mask: collision_mask,
        })
        .with(Velocity { x: 0.0, y: 0.0 })
        .with(LineOfSight {
            intersects: false,
            start: (0.0, 0.0),
            target: (0.0, 0.0),
        })
        .build();
}

pub const projectile_frames: &[Frame] = &[frame(0, 0, 25, 25)];
