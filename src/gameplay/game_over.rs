use specs::World;

use sdl2::pixels::Color;
use sdl2::rect::Rect;

use crate::gameplay::*;
use crate::render::*;
use crate::state::*;

use crate::main_menu::*;

use std::path::Path;

fn render_game_over(render_context: &mut RenderContext) {
    render_context.canvas.set_draw_color(Color::RGB(50, 50, 50));

    render_context
        .canvas
        .fill_rect(Rect::new(590, 345, 110, 25));

    if let Some(text) = render_context
        .font_manager
        .render("Game Over", Color::RGB(255, 0, 0))
    {
        let width = text.query().width;
        let height = text.query().height;

        render_context
            .canvas
            .copy(&text, None, Rect::new(600, 350, width, height));
    }
}

pub struct GameOverState {
    level_renderer: LevelRenderer,
    renderer: SpriteRenderer,
    timer_renderer: TimerRenderer,
    animation_system: AnimationSystem,
}

impl GameOverState {
    pub fn new() -> GameOverState {
        let mut level_renderer = LevelRenderer::new();
        let mut renderer = SpriteRenderer::new();
        let mut timer_renderer = TimerRenderer::new();
        let mut animation_system = AnimationSystem;

        GameOverState {
            level_renderer,
            renderer,
            timer_renderer,
            animation_system,
        }
    }
}

impl State for GameOverState {
    fn draw(&mut self, render_context: &mut RenderContext, world: &mut World) {
        self.level_renderer
            .render(render_context, world.system_data());
        self.renderer.render(render_context, world.system_data());
        self.timer_renderer
            .render(render_context, world.system_data());
        self.animation_system.run(world.system_data());
        render_game_over(render_context);
    }

    fn init(&mut self, world: &mut World) {}

    fn update(&mut self, world: &mut World) -> Transition {
        let mut restarting = false;
        let mut quit_to_menu = false;

        {
            let input = world.read_resource::<Input>();
            restarting = input.key_pressed.activate;
            quit_to_menu = input.key_pressed.back;
        }

        if quit_to_menu {
            return Transition::Replace(Box::new(MainMenuState::new()));
        }

        if restarting {
            world.delete_all();

            let current_level_path: PathBuf = {
                let path = world.read_resource::<CurrentLevelPath>();

                // Okay, so, we need to get this PathBuf out without
                // tripping up mutability rules for world.
                // So, we take an iterator over path.0, map it with a
                // clone closure and collect the treasures.
                //
                // It looks kinda weird, but this is the better option.
                path.0.iter().map(|p| p.clone()).collect()
            };

            match load_level(world, &current_level_path) {
                Err(e) => println!("Failed to load level: {}", e),
                Ok(()) => {
                    println!(
                        "Level loaded successfully: {}",
                        current_level_path.display()
                    );
                }
            }

            let mut timer = world.write_resource::<ElapsedTime>();

            timer.0 = 0.0;

            return Transition::Replace(Box::new(GameplayState::new()));
        }

        Transition::Continue
    }
}
