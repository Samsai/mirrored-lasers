use serde::{Deserialize, Serialize};

use specs::World;

use std::fs::File;
use std::io::Read;
use std::path::Path;

use crate::gameplay::entity_constructors::*;
use crate::gameplay::state_types::*;

pub fn load_level(world: &mut World, path: &Path) -> Result<(), std::io::Error> {
    let level_tiles = load_level_tiles(&path.join("map.txt"))?;
    println!("Tile loading succeeded for");
    world.insert(level_tiles);

    let mut game_state = GameState::default();

    let level_data = load_level_data(&path.join("metadata.json"))?;
    println!("Metadata loading succeeded for");

    let (player_x, player_y) = level_data.starting_location;
    create_player(world, player_x, player_y);

    game_state.robots = level_data.robots.len() as i32;

    for robot in level_data.robots {
        let (x, y) = robot.location;

        create_robot(world, x, y, robot.path);
    }

    for cargo in level_data.boxes {
        let (x, y) = cargo.location;

        create_cargo(world, x, y)
    }

    world.insert(game_state);

    Ok(())
}

#[derive(Serialize, Deserialize)]
pub struct Robot {
    location: (f32, f32),
    path: Vec<(f32, f32)>,
}

#[derive(Serialize, Deserialize)]
pub struct Cargo {
    location: (f32, f32),
}

#[derive(Serialize, Deserialize)]
pub struct LevelData {
    starting_location: (f32, f32),
    robots: Vec<Robot>,
    boxes: Vec<Cargo>,
}

fn load_level_data(path: &Path) -> Result<LevelData, std::io::Error> {
    let mut level_data_string = String::new();

    let mut file = File::open(path)?;
    file.read_to_string(&mut level_data_string);

    let level_data: LevelData = serde_json::from_str(&level_data_string).unwrap();

    Ok(level_data)
}

#[derive(Clone, Copy, PartialEq)]
pub enum Tile {
    Void,
    Floor,
    Mirror(u8),
    Wall,
    EnergyField,
    Splitter,
}

impl Tile {
    pub fn is_passable(&self) -> bool {
        match self {
            Tile::Void => false,
            Tile::Floor => true,
            Tile::Mirror(hp) => *hp == 0u8,
            Tile::Wall => false,
            Tile::EnergyField => true,
            Tile::Splitter => false,
        }
    }
}

#[derive(Default)]
pub struct Level {
    pub tiles: Vec<Vec<Tile>>,
    pub width: usize,
    pub height: usize,
}

impl Level {
    pub fn new(width: usize, height: usize) -> Level {
        let mut y_vec = Vec::with_capacity(height);

        for y in 0..height {
            let mut x_vec = Vec::with_capacity(width);

            for x in 0..width {
                x_vec.push(Tile::Floor);
            }

            y_vec.push(x_vec);
        }

        Level {
            tiles: y_vec,
            width,
            height,
        }
    }
}

fn load_level_tiles(path: &Path) -> Result<Level, std::io::Error> {
    println!("Trying to load level: {:?}", path);
    println!("In folder {:?}", std::env::current_dir());
    println!("Overall: {:?}", std::env::current_dir().unwrap().join(path));

    let mut level_string = String::new();

    let mut file = File::open(path)?;
    file.read_to_string(&mut level_string);

    let lines: Vec<&str> = level_string.lines().collect();

    let mut it = lines[0].split_whitespace();
    let width = it
        .next()
        .map(|s| s.trim().parse::<usize>().expect("Blarg"))
        .expect("blorg");
    let height = it
        .next()
        .map(|s| s.trim().parse::<usize>().expect("Blarg"))
        .expect("blorg");

    let mut level = Level::new(width, height);

    let mut x = 0;
    let mut y = 0;

    for i in 1..height + 1 {
        for c in lines[i].chars() {
            match c {
                'X' => level.tiles[y][x] = Tile::Void,
                ' ' => level.tiles[y][x] = Tile::Floor,
                'O' => level.tiles[y][x] = Tile::Mirror(4),
                '=' => level.tiles[y][x] = Tile::EnergyField,
                '$' => level.tiles[y][x] = Tile::Splitter,
                _ => level.tiles[y][x] = Tile::Wall,
            }
            x += 1;
        }
        x = 0;
        y += 1;
    }

    Ok(level)
}
