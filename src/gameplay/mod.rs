use specs::Dispatcher;
use specs::DispatcherBuilder;
use specs::Write;
use specs::{Entities, LazyUpdate};

use std::collections::HashSet;

use crate::*;

use crate::input::*;
use crate::render::*;
use crate::state::*;

pub mod level;
use level::*;

pub mod entity_constructors;
use entity_constructors::*;

mod state_types;

use state_types::GameState;

mod game_over;
use game_over::GameOverState;

mod victory;
use victory::VictoryState;

struct PlayerInputSystem;

impl<'a> System<'a> for PlayerInputSystem {
    type SystemData = (
        Read<'a, Input>,
        Read<'a, DeltaTime>,
        Write<'a, AudioEvents>,
        Entities<'a>,
        ReadStorage<'a, PlayerControlled>,
        ReadStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, FireRateLimited>,
        WriteStorage<'a, Sprite>,
        Read<'a, LazyUpdate>,
    );

    fn run(
        &mut self,
        (
            input,
            delta,
            mut audio,
            entities,
            player_controlled,
            positions,
            mut velocities,
            mut fire_rates,
            mut sprites,
            updater,
        ): Self::SystemData,
    ) {
        for (pc, pos, vel, fire_rate, sprite) in (
            &player_controlled,
            &positions,
            &mut velocities,
            &mut fire_rates,
            &mut sprites,
        )
            .join()
        {
            // TODO:    Consider changing this to a unit vector for
            //          consistent speed
            vel.y = input.mov_y * 200.0;
            vel.x = input.mov_x * 200.0;

            sprite.animation.frames = player_idle_frames;

            if vel.x != 0.0 || vel.y != 0.0 {
                sprite.animation.frames = player_walking_frames;
            }

            if vel.x < 0.0 {
                sprite.animation.frames = player_walking_frames;
                sprite.flipped = true;
            } else if vel.x > 0.0 {
                sprite.animation.frames = player_walking_frames;
                sprite.flipped = false;
            }

            if fire_rate.timer > 0.0 {
                fire_rate.timer -= delta.as_secs_f32();
            }

            if input.key_down.fire && fire_rate.timer <= 0.0 {
                sprite.animation.frames = player_firing_frames;
                sprite.animation.reset();

                fire_rate.timer = fire_rate.rate;

                let (rel_x, rel_y) = if input.gamepad_enabled {
                    // Skip if the player isn't aiming
                    if input.aim_rel_x == 0.0 && input.aim_rel_y == 0.0 {
                        return;
                    }
                    (input.aim_rel_x, input.aim_rel_y)
                } else {
                    (input.mouse_x as f32 - pos.x, input.mouse_y as f32 - pos.y)
                };

                if rel_x < 0.0 {
                    sprite.flipped = true;
                } else {
                    sprite.flipped = false;
                }

                let hypot = rel_x.hypot(rel_y);

                let sin = rel_x / hypot;
                let cos = rel_y / hypot;

                let mut sprite_effects = SpriteEffects::new();
                sprite_effects.insert(SpriteEffect::AlphaMod(200));
                sprite_effects.insert(SpriteEffect::ColourMod(100, 100, 255));

                let projectile = updater
                    .create_entity(&entities)
                    .with(Sprite {
                        name: String::from("projectile"),
                        animation: Animation::new(projectile_frames, 0.0),
                        timer: 0.0,
                        rotation: cos.atan2(sin).to_degrees() as f64,
                        flipped: false,
                        effects: sprite_effects,
                    })
                    .with(pos.clone())
                    .with(Collideable {
                        width: 20.0,
                        height: 20.0,
                        layer: CollisionLayer::Projectile,
                        mask: HashSet::new(),
                    })
                    .with(Velocity {
                        x: sin * 800.0,
                        y: cos * 800.0,
                    })
                    .with(Projectile {
                        timer: 3.0,
                        num_splits: 5,
                        out_of_phase: true,
                        entered_phase_field: false,
                    })
                    .build();

                audio.list.push(String::from("pew"));
            }
        }
    }
}

struct UpdatePositionsSystem;

impl<'a> System<'a> for UpdatePositionsSystem {
    type SystemData = (
        Read<'a, DeltaTime>,
        Read<'a, Level>,
        Entities<'a>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, Position>,
        ReadStorage<'a, Collideable>,
        Read<'a, LazyUpdate>,
    );

    fn run(
        &mut self,
        (delta, level, entities, mut velocities, mut positions, collideables, updater): Self::SystemData,
    ) {
        for (entity, vel, pos, col) in
            (&entities, &mut velocities, &mut positions, &collideables).join()
        {
            let new_x = pos.x + (vel.x as f64 * delta.as_secs_f64()) as f32;
            let new_y = pos.y + (vel.y as f64 * delta.as_secs_f64()) as f32;

            // Check movement along X axis
            let mut has_collided = false;

            let mut tile = None;
            let mut tile_coordinate_x = 0;
            let mut tile_coordinate_y = 0;

            for y_offset in 0..2 {
                for x_offset in 0..2 {
                    let (sample_x, sample_y) = (
                        new_x + (col.width / 2.0 * x_offset as f32),
                        pos.y + (col.height / 2.0 * y_offset as f32),
                    );

                    let (tile_x, tile_y) = ((sample_x / 35.0) as usize, (sample_y / 35.0) as usize);

                    if !level.tiles[tile_y][tile_x].is_passable() {
                        has_collided = true;

                        tile = Some(level.tiles[tile_y][tile_x]);
                        tile_coordinate_x = tile_x;
                        tile_coordinate_y = tile_y;
                    }
                }
            }

            if !has_collided {
                pos.x = new_x;
            } else {
                updater.insert(
                    entity,
                    WallCollision {
                        tile: tile.unwrap(),
                        vertical: false,
                        x: tile_coordinate_x,
                        y: tile_coordinate_y,
                    },
                );
            }

            // Check movement along Y axis
            has_collided = false;

            for y_offset in 0..2 {
                for x_offset in 0..2 {
                    let (sample_x, sample_y) = (
                        pos.x + (col.width / 2.0 * x_offset as f32),
                        new_y + (col.height / 2.0 * y_offset as f32),
                    );

                    let (tile_x, tile_y) = ((sample_x / 35.0) as usize, (sample_y / 35.0) as usize);

                    if !level.tiles[tile_y][tile_x].is_passable() {
                        has_collided = true;
                        tile = Some(level.tiles[tile_y][tile_x]);
                        tile_coordinate_x = tile_x;
                        tile_coordinate_y = tile_y;
                    }
                }
            }

            if !has_collided {
                pos.y = new_y;
            } else {
                updater.insert(
                    entity,
                    WallCollision {
                        tile: tile.unwrap(),
                        vertical: true,
                        x: tile_coordinate_x,
                        y: tile_coordinate_y,
                    },
                );
            }
        }
    }
}

struct EntityCollisionSystem;

impl<'a> System<'a> for EntityCollisionSystem {
    type SystemData = (
        Read<'a, DeltaTime>,
        Entities<'a>,
        WriteStorage<'a, Velocity>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Collideable>,
        Read<'a, LazyUpdate>,
    );

    fn run(
        &mut self,
        (delta, entities, mut velocities, positions, collideables, updater): Self::SystemData,
    ) {
        for (entity, vel, pos, col) in
            (&entities, &mut velocities, &positions, &collideables).join()
        {
            let new_x = pos.x + (vel.x as f64 * delta.as_secs_f64()) as f32;
            let new_y = pos.y + (vel.y as f64 * delta.as_secs_f64()) as f32;

            // Check movement along X axis
            let mut has_collided = false;

            for (other_entity, other_pos, other_col) in
                (&entities, &positions, &collideables).join()
            {
                if entity != other_entity && col.mask.get(&other_col.layer).is_some() {
                    let col_rect1 = Rect::new(
                        new_x as i32,
                        pos.y as i32,
                        col.width as u32,
                        col.height as u32,
                    );
                    let col_rect2 = Rect::new(
                        other_pos.x as i32,
                        other_pos.y as i32,
                        other_col.width as u32,
                        other_col.height as u32,
                    );

                    if col_rect1.has_intersection(col_rect2) {
                        updater.insert(
                            entity,
                            Collision {
                                collider: other_entity,
                            },
                        );
                        has_collided = true;
                    }
                }
            }

            if has_collided {
                vel.x *= 0.0;
            }

            // Check movement along Y axis
            has_collided = false;

            for (other_entity, other_pos, other_col) in
                (&entities, &positions, &collideables).join()
            {
                if entity != other_entity && col.mask.contains(&other_col.layer) {
                    let col_rect1 = Rect::new(
                        new_x as i32,
                        new_y as i32,
                        col.width as u32,
                        col.height as u32,
                    );
                    let col_rect2 = Rect::new(
                        other_pos.x as i32,
                        other_pos.y as i32,
                        other_col.width as u32,
                        other_col.height as u32,
                    );

                    if col_rect1.has_intersection(col_rect2) {
                        updater.insert(
                            entity,
                            Collision {
                                collider: other_entity,
                            },
                        );
                        has_collided = true;
                    }
                }
            }

            if has_collided {
                vel.y *= 0.0;
            }
        }
    }
}

struct ProjectileSystem;

impl<'a> System<'a> for ProjectileSystem {
    type SystemData = (
        Read<'a, DeltaTime>,
        Entities<'a>,
        ReadStorage<'a, PlayerControlled>,
        WriteStorage<'a, Enemy>,
        WriteStorage<'a, Crate>,
        Write<'a, Level>,
        WriteStorage<'a, Projectile>,
        WriteStorage<'a, Velocity>,
        ReadStorage<'a, Position>,
        WriteStorage<'a, Sprite>,
        WriteStorage<'a, Collideable>,
        ReadStorage<'a, Collision>,
        ReadStorage<'a, WallCollision>,
        Write<'a, GameState>,
        Write<'a, AudioEvents>,
        Read<'a, LazyUpdate>,
    );

    fn run(
        &mut self,
        (
            delta,
            entities,
            pcs,
            mut enemies,
            mut crates,
            mut level,
            mut projectiles,
            mut velocities,
            positions,
            mut sprites,
            mut collideables,
            collisions,
            wall_collisions,
            mut game_state,
            mut audio,
            updater,
        ): Self::SystemData,
    ) {
        for (entity, projectile, position, collideable, sprite) in (
            &entities,
            &mut projectiles,
            &positions,
            &mut collideables,
            &mut sprites,
        )
            .join()
        {
            projectile.timer -= delta.as_secs_f64() as f32;

            if projectile.timer <= 0.0 {
                entities.delete(entity);
            }

            let (tile_x, tile_y) = position.to_tile_coordinate();

            if projectile.entered_phase_field && level.tiles[tile_y][tile_x] != Tile::EnergyField {
                projectile.entered_phase_field = false;
            } else if !projectile.entered_phase_field
                && level.tiles[tile_y][tile_x] == Tile::EnergyField
            {
                projectile.entered_phase_field = true;

                if projectile.out_of_phase {
                    projectile.out_of_phase = false;
                    collideable.mask.insert(CollisionLayer::Player);
                    collideable.mask.insert(CollisionLayer::Enemy);
                    sprite.effects.clear();
                } else {
                    projectile.out_of_phase = true;
                    collideable.mask.remove(&CollisionLayer::Player);
                    collideable.mask.remove(&CollisionLayer::Enemy);
                    sprite
                        .effects
                        .insert(SpriteEffect::ColourMod(100, 100, 255));
                    sprite.effects.insert(SpriteEffect::AlphaMod(200));
                }
            }
        }

        for (entity, projectile, collision) in (&entities, &projectiles, &collisions).join() {
            if let Some(player) = pcs.get(collision.collider) {
                game_state.player_dead = true;
            }

            if let Some(enemy) = enemies.get_mut(collision.collider) {
                enemy.hit = true;

                if let Some(mut enemy_collideable) = collideables.get_mut(collision.collider) {
                    enemy_collideable.layer = CollisionLayer::Dead;
                }
            }

            if let Some(cargo) = crates.get_mut(collision.collider) {
                cargo.hit = true;
            }

            entities.delete(entity);
        }

        for (entity, projectile, velocity, position, collideable, sprite, wall_collision) in (
            &entities,
            &mut projectiles,
            &mut velocities,
            &positions,
            &mut collideables,
            &mut sprites,
            &wall_collisions,
        )
            .join()
        {
            if let Tile::Mirror(hp) = wall_collision.tile {
                updater.remove::<WallCollision>(entity);

                audio.list.push(String::from("ricochet"));

                if wall_collision.vertical {
                    velocity.y *= -1.0;
                } else {
                    velocity.x *= -1.0;
                }

                if projectile.out_of_phase {
                    projectile.out_of_phase = false;
                    collideable.mask.insert(CollisionLayer::Player);
                    collideable.mask.insert(CollisionLayer::Enemy);
                    sprite.effects.clear();
                } else {
                    projectile.out_of_phase = true;
                    collideable.mask.remove(&CollisionLayer::Player);
                    collideable.mask.remove(&CollisionLayer::Enemy);
                    sprite
                        .effects
                        .insert(SpriteEffect::ColourMod(100, 100, 255));
                    sprite.effects.insert(SpriteEffect::AlphaMod(200));
                }

                sprite.rotation = velocity.y.atan2(velocity.x).to_degrees() as f64;

                level.tiles[wall_collision.y][wall_collision.x] = Tile::Mirror(hp - 1);

                if hp - 1 == 0 {
                    audio.list.push(String::from("shatter"));
                }
            } else if let Tile::Splitter = wall_collision.tile {
                level.tiles[wall_collision.y][wall_collision.x] = Tile::Floor;

                if projectile.num_splits > 0 {
                    let rot0 = sprite.rotation;
                    let spread: f32 = 5.0; // 5 degree spread
                    let rotations = [-spread.to_radians(), 0.0, spread.to_radians()];

                    for rot in rotations.iter() {
                        let x_vel = rot.cos() * velocity.x - rot.sin() * velocity.y;
                        let y_vel = rot.sin() * velocity.x + rot.cos() * velocity.y;

                        let new_pos = Position {
                            x: position.x + velocity.x * delta.as_secs_f32() * 3.0,
                            y: position.y + velocity.y * delta.as_secs_f32() * 3.0,
                        };

                        let mut sprite_effects = SpriteEffects::new();

                        for effect in sprite.effects.iter() {
                            sprite_effects.insert(effect.clone());
                        }

                        let mut collision_mask = CollisionMask::new();

                        for layer in collideable.mask.iter() {
                            collision_mask.insert(layer.clone());
                        }

                        let projectile = updater
                            .create_entity(&entities)
                            .with(Sprite {
                                name: String::from("projectile"),
                                animation: Animation::new(projectile_frames, 0.0),
                                timer: 0.0,
                                rotation: rot0 + *rot as f64,
                                flipped: false,
                                effects: sprite_effects,
                            })
                            .with(new_pos)
                            .with(Collideable {
                                width: 20.0,
                                height: 20.0,
                                layer: CollisionLayer::Projectile,
                                mask: collision_mask,
                            })
                            .with(Velocity { x: x_vel, y: y_vel })
                            .with(Projectile {
                                timer: 3.0,
                                num_splits: projectile.num_splits - 1,
                                out_of_phase: projectile.out_of_phase,
                                entered_phase_field: projectile.entered_phase_field,
                            })
                            .build();
                    }
                }

                entities.delete(entity);
            } else {
                entities.delete(entity);
            }
        }
    }
}

struct CargoDestructionSystem;

impl<'a> System<'a> for CargoDestructionSystem {
    type SystemData = (
        Read<'a, DeltaTime>,
        Write<'a, GameState>,
        Write<'a, AudioEvents>,
        Entities<'a>,
        WriteStorage<'a, Crate>,
        WriteStorage<'a, Sprite>,
    );

    fn run(
        &mut self,
        (delta, mut game_state, mut audio, entities, mut crates, mut sprites): Self::SystemData,
    ) {
        for (entity, cargo, sprite) in (&entities, &mut crates, &mut sprites).join() {
            if cargo.hit {
                cargo.dead = true;
            }

            // TODO: Add crate destruction sprites and sounds here
            if cargo.dead {
                cargo.death_timer -= delta.as_secs_f32();

                if cargo.death_timer < 0.0 {
                    entities.delete(entity);
                }
            }
        }
    }
}

struct EnemySystem;

impl<'a> System<'a> for EnemySystem {
    type SystemData = (
        Read<'a, DeltaTime>,
        Write<'a, GameState>,
        Write<'a, AudioEvents>,
        Entities<'a>,
        ReadStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, Enemy>,
        WriteStorage<'a, Sprite>,
    );

    fn run(
        &mut self,
        (
            delta,
            mut game_state,
            mut audio,
            entities,
            positions,
            mut velocities,
            mut enemies,
            mut sprites,
        ): Self::SystemData,
    ) {
        for (entity, pos, vel, enemy, sprite) in (
            &entities,
            &positions,
            &mut velocities,
            &mut enemies,
            &mut sprites,
        )
            .join()
        {
            if enemy.hit {
                if !enemy.dead {
                    game_state.robots -= 1;
                }

                enemy.dead = true;
                enemy.hit = false;
                sprite.animation.frames = robot_death_frames;
                sprite.animation.reset();

                audio.list.push(String::from("robot_death"));

                vel.x = 0.0;
                vel.y = 0.0;
            }

            if enemy.dead {
                enemy.death_timer -= delta.as_secs_f32();

                if enemy.death_timer <= 0.0 {
                    entities.delete(entity);
                }
            } else {
                let (node_x, node_y) = enemy.path[enemy.target];
                let (rel_x, rel_y) = (node_x - pos.x, node_y - pos.y);

                let distance = rel_x.hypot(rel_y);

                if distance < 5.0 {
                    enemy.target += 1;

                    if enemy.target >= enemy.path.len() {
                        enemy.target = 0;
                    }
                } else {
                    let sin = rel_x / distance;
                    let cos = rel_y / distance;

                    vel.x = sin * 200.0;
                    vel.y = cos * 200.0;
                }
            }
        }
    }
}

fn cast_ray(
    level: &Level,
    (start_x, start_y): (f32, f32),
    (player_x, player_y): (f32, f32),
    angle: f32,
) -> (bool, (f32, f32)) {
    let dx = angle.to_radians().cos();
    let dy = angle.to_radians().sin();

    let mut x = start_x;
    let mut y = start_y;

    'ray: loop {
        x += dx;
        y += dy;

        let tile_x = (x / 35.0) as usize;
        let tile_y = (y / 35.0) as usize;

        if !level.tiles[tile_y][tile_x].is_passable() {
            return (false, (x, y));
        }

        let player_rect = Rect::from_center((player_x as i32, player_y as i32), 32, 32);

        if player_rect.contains_point((x as i32, y as i32)) {
            return (true, (x, y));
        }
    }
}

struct LineOfSightSystem;

impl<'a> System<'a> for LineOfSightSystem {
    type SystemData = (
        Read<'a, DeltaTime>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, PlayerControlled>,
        WriteStorage<'a, Sprite>,
        WriteStorage<'a, Enemy>,
        Read<'a, Level>,
        Write<'a, GameState>,
        WriteStorage<'a, LineOfSight>,
    );

    fn run(
        &mut self,
        (
            delta,
            positions,
            players,
            mut sprites,
            mut enemies,
            level,
            mut game_state,
            mut line_of_sights,
        ): Self::SystemData,
    ) {
        (&positions, &players, &mut sprites).join().next().map(
            |(player_pos, player, player_sprite)| {
                for (pos, los, enemy) in (&positions, &mut line_of_sights, &mut enemies).join() {
                    if !enemy.dead {
                        let (intersects, (x, y)) = cast_ray(
                            &level,
                            (pos.x, pos.y),
                            (player_pos.x, player_pos.y),
                            enemy.aim,
                        );

                        los.start = (pos.x, pos.y);
                        los.target = (x, y);
                        los.intersects = intersects;

                        if (los.intersects) {
                            game_state.player_dead = true;
                        }

                        enemy.aim += 400.0 * delta.as_secs_f32();
                        enemy.aim %= 360.0;
                    }
                }
            },
        );
    }
}

struct TimerSystem;

impl<'a> System<'a> for TimerSystem {
    type SystemData = (Read<'a, DeltaTime>, Write<'a, ElapsedTime>);

    fn run(&mut self, (delta, mut elapsed): Self::SystemData) {
        elapsed.0 += delta.as_secs_f32();
    }
}

struct DeathSystem;

impl<'a> System<'a> for DeathSystem {
    type SystemData = (
        Read<'a, GameState>,
        Write<'a, AudioEvents>,
        ReadStorage<'a, PlayerControlled>,
        WriteStorage<'a, Sprite>,
    );

    fn run(&mut self, (state, mut audio, pcs, mut sprites): Self::SystemData) {
        if state.player_dead {
            (&pcs, &mut sprites).join().next().map(|(player, sprite)| {
                sprite.animation.frames = player_death_frames;
                sprite.animation.animation_transition = None;
                sprite.animation.reset();
            });
            audio.list.push(String::from("death"));
        }
    }
}

pub struct GameplayState<'a, 'b> {
    dispatcher: Dispatcher<'a, 'b>,
    level_renderer: LevelRenderer,
    los_renderer: LOSRenderer,
    renderer: SpriteRenderer,
    timer_renderer: TimerRenderer,
    player_aim_renderer: PlayerAimRenderer,
}

impl<'a, 'b> GameplayState<'a, 'b> {
    pub fn new() -> GameplayState<'a, 'b> {
        let mut dispatcher = DispatcherBuilder::new()
            .with(PlayerInputSystem, "player_input", &[])
            .with(EnemySystem, "enemies", &[])
            .with(CargoDestructionSystem, "cargo", &[])
            .with(EntityCollisionSystem, "entity_collisions", &[])
            .with(
                UpdatePositionsSystem,
                "update_positions",
                &["player_input", "enemies", "entity_collisions"],
            )
            .with(LineOfSightSystem, "los", &["update_positions"])
            .with(ProjectileSystem, "projectiles", &["entity_collisions"])
            .with(
                DeathSystem,
                "death",
                &["entity_collisions", "los", "projectiles"],
            )
            .with(AnimationSystem, "animation", &["player_input"])
            .with(TimerSystem, "timer", &[])
            .build();

        let mut level_renderer = LevelRenderer::new();
        let mut los_renderer = LOSRenderer::new();
        let mut renderer = SpriteRenderer::new();
        let mut player_aim_renderer = PlayerAimRenderer::new();
        let mut timer_renderer = TimerRenderer::new();

        GameplayState {
            dispatcher,
            level_renderer,
            los_renderer,
            renderer,
            timer_renderer,
            player_aim_renderer,
        }
    }
}

impl<'a, 'b> State for GameplayState<'a, 'b> {
    fn draw(&mut self, render_context: &mut RenderContext, world: &mut World) {
        self.level_renderer
            .render(render_context, world.system_data());
        self.renderer.render(render_context, world.system_data());
        self.los_renderer
            .render(render_context, world.system_data());
        self.player_aim_renderer
            .render(render_context, world.system_data());
        self.timer_renderer
            .render(render_context, world.system_data());
    }

    fn init(&mut self, world: &mut World) {}

    fn update(&mut self, world: &mut World) -> Transition {
        {
            let mut game_state = world.read_resource::<GameState>();

            if game_state.player_dead {
                return Transition::Replace(Box::new(GameOverState::new()));
            }

            if game_state.robots <= 0 {
                return Transition::Replace(Box::new(VictoryState::new()));
            }
        }

        self.dispatcher.dispatch(world);
        return Transition::Continue;
    }
}
