#[derive(Default)]
pub struct GameState {
    pub player_dead: bool,
    pub robots: i32,
}
