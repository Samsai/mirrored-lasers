use specs::World;

use sdl2::pixels::Color;
use sdl2::rect::Rect;

use specs::Dispatcher;
use specs::DispatcherBuilder;

use crate::gameplay::*;
use crate::main_menu::level_selection::LevelSelectionState;
use crate::render::*;
use crate::state::*;

use std::io::Write;
use std::path::Path;

fn render_victory(render_context: &mut RenderContext) {
    render_context.canvas.set_draw_color(Color::RGB(50, 50, 50));

    render_context
        .canvas
        .fill_rect(Rect::new(590, 345, 100, 25));

    if let Some(text) = render_context
        .font_manager
        .render("VICTORY!", Color::RGB(0, 200, 0))
    {
        let width = text.query().width;
        let height = text.query().height;

        render_context
            .canvas
            .copy(&text, None, Rect::new(600, 350, width, height));
    }
}

pub struct VictoryState<'a, 'b> {
    level_renderer: LevelRenderer,
    renderer: SpriteRenderer,
    timer_renderer: TimerRenderer,
    dispatcher: Dispatcher<'a, 'b>,
}

impl<'a, 'b> VictoryState<'a, 'b> {
    pub fn new() -> VictoryState<'a, 'b> {
        let mut level_renderer = LevelRenderer::new();
        let mut renderer = SpriteRenderer::new();
        let mut timer_renderer = TimerRenderer::new();
        let mut dispatcher = DispatcherBuilder::new()
            .with(EnemySystem, "enemies", &[])
            .with(AnimationSystem, "animation", &[])
            .build();

        VictoryState {
            level_renderer,
            renderer,
            timer_renderer,
            dispatcher,
        }
    }
}

impl<'a, 'b> State for VictoryState<'a, 'b> {
    fn draw(&mut self, render_context: &mut RenderContext, world: &mut World) {
        self.level_renderer
            .render(render_context, world.system_data());
        self.renderer.render(render_context, world.system_data());
        self.timer_renderer
            .render(render_context, world.system_data());
        render_victory(render_context);
    }

    fn init(&mut self, world: &mut World) {
        let mut game_save = world.write_resource::<GameSave>();
        let current_level_path = world.read_resource::<CurrentLevelPath>();
        let elapsed_time = world.read_resource::<ElapsedTime>();

        let level_path: PathBuf = current_level_path.0.iter().map(|p| p.clone()).collect();

        if let Some(previous_time) = game_save.level_times.get(&level_path) {
            if *previous_time > elapsed_time.0 {
                game_save.level_times.insert(level_path, elapsed_time.0);
            }
        } else {
            game_save.level_times.insert(level_path, elapsed_time.0);
        }

        let mut file = File::create("save.txt").expect("Couldn't open save file!");
        let string = serde_json::to_string(&*game_save).unwrap(); // Yeah... I totally did reference a dereference

        write!(file, "{}", string);
    }

    fn update(&mut self, world: &mut World) -> Transition {
        let mut restarting = false;

        {
            let input = world.read_resource::<Input>();
            restarting = input.key_pressed.activate;
        }

        if restarting {
            return Transition::Replace(Box::new(LevelSelectionState::new()));
        }

        self.dispatcher.dispatch(world);

        Transition::Continue
    }
}
