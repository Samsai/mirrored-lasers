use sdl2::controller::Axis;
use sdl2::controller::Button;
use sdl2::controller::GameController;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::mouse::MouseButton;
use sdl2::mouse::MouseState;

const GAMEPAD_REPEAT_TIME: u32 = 150;

const I16_MAX: i16 = 32767;

pub fn keyboard_handler(input: &mut Input, event: &Event) {
    match event {
        Event::KeyDown {
            keycode: Some(key), ..
        } => {
            input.gamepad_enabled = false;

            match key {
                Keycode::W | Keycode::Up => {
                    input.key_pressed.up = true;
                    input.key_down.up = true;
                }
                Keycode::S | Keycode::Down => {
                    input.key_pressed.down = true;
                    input.key_down.down = true;
                }
                Keycode::A | Keycode::Left => {
                    input.key_pressed.left = true;
                    input.key_down.left = true;
                }
                Keycode::D | Keycode::Right => {
                    input.key_pressed.right = true;
                    input.key_down.right = true;
                }
                Keycode::Space => {
                    input.key_pressed.fire = true;
                    input.key_down.fire = true;
                }
                Keycode::Return => {
                    input.key_pressed.activate = true;
                    input.key_down.activate = true;
                }
                Keycode::Escape => {
                    input.key_pressed.back = true;
                    input.key_down.back = true;
                }
                _ => (),
            }
        }
        Event::KeyUp {
            keycode: Option::Some(key),
            ..
        } => match key {
            Keycode::W | Keycode::Up => {
                input.key_down.up = false;
            }
            Keycode::S | Keycode::Down => {
                input.key_down.down = false;
            }
            Keycode::A | Keycode::Left => {
                input.key_down.left = false;
            }
            Keycode::D | Keycode::Right => {
                input.key_down.right = false;
            }
            Keycode::Space => {
                input.key_down.fire = false;
            }
            Keycode::Return => {
                input.key_down.activate = false;
            }
            _ => (),
        },
        _ => (),
    }

    if input.key_down.left {
        input.mov_x = -1.0;
    } else if input.key_down.right {
        input.mov_x = 1.0;
    } else {
        input.mov_x = 0.0;
    }

    if input.key_down.up {
        input.mov_y = -1.0;
    } else if input.key_down.down {
        input.mov_y = 1.0;
    } else {
        input.mov_y = 0.0;
    }
}

pub fn mouse_handler(input: &mut Input, event: &Event) {
    match event {
        Event::MouseButtonDown { mouse_btn, .. } => {
            if let MouseButton::Left = mouse_btn {
                input.key_pressed.fire = true;
                input.key_down.fire = true;
            }
        }
        Event::MouseButtonUp { mouse_btn, .. } => {
            if let MouseButton::Left = mouse_btn {
                input.key_down.fire = false;
            }
        }
        Event::MouseMotion {
            mousestate, x, y, ..
        } => {
            input.mouse_x = *x;
            input.mouse_y = *y;
        }
        _ => (),
    }
}

pub fn gamepad_handler(input: &mut Input, event: &Event) {
    input.gamepad_enabled = true;

    match event {
        Event::ControllerButtonDown {
            timestamp, button, ..
        } => match button {
            Button::A => {
                if timestamp - input.gamepad_repeat_timers.activate > GAMEPAD_REPEAT_TIME {
                    input.key_pressed.activate = true;
                    input.key_down.activate = true;
                    input.gamepad_repeat_timers.activate = *timestamp;
                }
            }
            Button::B => {
                if timestamp - input.gamepad_repeat_timers.activate > GAMEPAD_REPEAT_TIME {
                    input.key_pressed.back = true;
                    input.key_down.back = true;
                    input.gamepad_repeat_timers.back = *timestamp;
                }
            }
            _ => (),
        },
        Event::ControllerAxisMotion {
            timestamp,
            axis,
            value,
            ..
        } => match axis {
            Axis::RightX => {
                let aim_x_axis = *value as i32;
                input.aim_rel_x = aim_x_axis as f32 / I16_MAX as f32;
            }
            Axis::RightY => {
                let aim_y_axis = *value as i32;
                input.aim_rel_y = aim_y_axis as f32 / I16_MAX as f32;
            }
            Axis::LeftX => {
                let x_axis = *value;

                input.mov_x = x_axis as f32 / I16_MAX as f32;

                if x_axis < -1000 {
                    if timestamp - input.gamepad_repeat_timers.left > GAMEPAD_REPEAT_TIME {
                        input.key_pressed.left = true;
                        input.gamepad_repeat_timers.left = *timestamp;
                    }
                } else if x_axis > 1000 {
                    if timestamp - input.gamepad_repeat_timers.right > GAMEPAD_REPEAT_TIME {
                        input.key_pressed.right = true;
                        input.gamepad_repeat_timers.right = *timestamp;
                    }
                } else {
                    input.key_pressed.left = false;
                    input.key_pressed.right = false;
                }
            }
            Axis::LeftY => {
                let y_axis = *value;

                input.mov_y = y_axis as f32 / I16_MAX as f32;

                if y_axis < -1000 {
                    if timestamp - input.gamepad_repeat_timers.up > GAMEPAD_REPEAT_TIME {
                        input.key_pressed.up = true;
                        input.gamepad_repeat_timers.up = *timestamp;
                    }
                } else if y_axis > 1000 {
                    if timestamp - input.gamepad_repeat_timers.down > GAMEPAD_REPEAT_TIME {
                        input.key_pressed.down = true;
                        input.gamepad_repeat_timers.down = *timestamp;
                    }
                } else {
                    input.key_pressed.up = false;
                    input.key_pressed.down = false;
                }
            }
            Axis::TriggerRight => {
                let trigger_axis = *value;

                if trigger_axis > 1000 {
                    input.key_down.fire = true;
                } else {
                    input.key_down.fire = false;
                }
            }
            _ => (),
        },
        _ => (),
    }
}

#[derive(Default)]
pub struct Input {
    pub gamepad_enabled: bool,
    gamepad_repeat_timers: GamepadRepeatTimers,
    pub key_down: Keys,
    pub key_pressed: Keys,
    pub mov_x: f32,
    pub mov_y: f32,
    pub aim_rel_x: f32,
    pub aim_rel_y: f32,
    pub mouse_x: i32,
    pub mouse_y: i32,
}

#[derive(Default)]
struct GamepadRepeatTimers {
    activate: u32,
    back: u32,
    left: u32,
    right: u32,
    up: u32,
    down: u32,
}

#[derive(Default)]
pub struct Keys {
    pub up: bool,
    pub down: bool,
    pub left: bool,
    pub right: bool,
    pub fire: bool,
    pub activate: bool,
    pub back: bool,
}

impl Keys {
    pub fn new() -> Keys {
        Keys {
            up: false,
            down: false,
            left: false,
            right: false,
            fire: false,
            activate: false,
            back: false,
        }
    }

    pub fn reset(&mut self) {
        self.up = false;
        self.down = false;
        self.left = false;
        self.right = false;
        self.fire = false;
        self.activate = false;
        self.back = false;
    }
}

impl Input {
    pub fn new() -> Input {
        Input {
            gamepad_enabled: false,
            gamepad_repeat_timers: GamepadRepeatTimers::default(),
            key_down: Keys::new(),
            key_pressed: Keys::new(),
            mov_x: 0.0,
            mov_y: 0.0,
            aim_rel_x: 0.0,
            aim_rel_y: 0.0,
            mouse_x: 0,
            mouse_y: 0,
        }
    }

    pub fn reset(&mut self) {
        self.key_pressed.reset();
    }
}
