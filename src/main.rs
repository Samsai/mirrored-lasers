extern crate sdl2;
extern crate specs;

extern crate serde;
extern crate serde_json;

use std::path::Path;
use std::path::PathBuf;
use std::time::*;

use std::fs::File;
use std::io::Read as OtherRead;
use std::io::Write;

use sdl2::event::Event;
use sdl2::keyboard::KeyboardState;
use sdl2::keyboard::Keycode;
use sdl2::mouse::MouseButton;
use sdl2::mouse::MouseState;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::video::Window;

use sdl2::mixer::*;

use sdl2::filesystem::pref_path;

use specs::Join;
use specs::{Builder, World, WorldExt};
use specs::{Dispatcher, DispatcherBuilder};
use specs::{Read, ReadStorage, System, WriteStorage};

use serde::{Deserialize, Serialize};

mod state;
use state::*;

mod component;
use component::*;

mod texture_manager;
use texture_manager::*;

mod sfx_manager;
use sfx_manager::*;

mod font_manager;
use font_manager::*;

mod main_menu;
use main_menu::MainMenuState;

mod gameplay;
use gameplay::GameplayState;

use gameplay::level::load_level;
use gameplay::level::Level;

mod render;
use render::RenderContext;
use render::SpriteRenderer;

mod input;
use input::Input;
use input::*;

type DeltaTime = std::time::Duration;

enum GameEngineMessage {
    ToggleFullscreen,
}

#[derive(Default)]
struct MessageQueue(Vec<GameEngineMessage>);

#[derive(Default)]
struct ElapsedTime(f32);

#[derive(Default)]
struct CurrentLevelPath(Option<PathBuf>);

#[derive(Default, Deserialize, Serialize)]
pub struct GameSave {
    pub level_times: std::collections::HashMap<PathBuf, f32>,
    pub last_level_index: usize,
    pub robots_destroyed: usize,
}

impl GameSave {
    pub fn save(&self) {
        let save_path = PathBuf::from(pref_path("Samsai", "robot-ricochet").expect("Failed to get preferred path!"));

        if !save_path.exists() {
            std::fs::create_dir(&save_path);
        }

        let mut file = File::create(save_path.join("save.txt")).expect("Couldn't open save file!");
        let string = serde_json::to_string(self).unwrap();

        write!(file, "{}", string);
    }

    pub fn load() -> GameSave {
        let save_path = PathBuf::from(pref_path("Samsai", "robot-ricochet").expect("Failed to get preferred path!"));

        if let Ok(mut file) = File::open(save_path.join("save.txt")) {
            let mut save_string = String::new();
            file.read_to_string(&mut save_string)
                .expect("Reading save failed!");

            let game_save: GameSave = serde_json::from_str(&save_string).unwrap();

            println!("Save file loaded.");

            return game_save;
        } else {
            GameSave::default()
        }
    }
}

#[derive(Deserialize, Serialize)]
pub struct GameConfigs {
    pub sfx_volume: i32,
    pub music_volume: i32,
    pub controller_index: usize,
}

impl GameConfigs {
    pub fn new() -> GameConfigs {
        GameConfigs {
            sfx_volume: 128,
            music_volume: 128,
            controller_index: 0,
        }
    }

    pub fn save(&self) {
        let config_path = PathBuf::from(pref_path("Samsai", "robot-ricochet").expect("Failed to get preferred path!"));

        if !config_path.exists() {
            std::fs::create_dir(&config_path);
        }

        let mut file = File::create(config_path.join("configs.txt")).expect("Couldn't open save file!");
        let string = serde_json::to_string(self).unwrap(); // Yeah... I totally did reference a dereference

        write!(file, "{}", string);
    }

    pub fn load() -> GameConfigs {
        let config_path = PathBuf::from(pref_path("Samsai", "robot-ricochet").expect("Failed to get preferred path!"));

        if let Ok(mut file) = File::open(config_path.join("configs.txt")) {
            let mut config_string = String::new();
            file.read_to_string(&mut config_string)
                .expect("Reading configs failed!");

            let game_configs: GameConfigs = serde_json::from_str(&config_string).unwrap();

            return game_configs;
        } else {
            GameConfigs::default()
        }
    }
}

impl Default for GameConfigs {
    fn default() -> GameConfigs {
        GameConfigs::new()
    }
}

fn main() {
    let sdl2_context = sdl2::init().unwrap();

    println!("{}", sdl2::filesystem::base_path().unwrap());

    let _audio = sdl2_context.audio().unwrap();

    sdl2::mixer::open_audio(44100, AUDIO_S16LSB, DEFAULT_CHANNELS, 1024);
    let mixer_context = sdl2::mixer::init(InitFlag::OGG).unwrap();

    sdl2::mixer::allocate_channels(16);

    let ttf_context = sdl2::ttf::init().unwrap();

    let video_subsystem = sdl2_context.video().unwrap();
    let window = video_subsystem
        .window("Mirrored Lasers", 1280, 720)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas: Canvas<Window> = window
        .into_canvas()
        .accelerated()
        .present_vsync()
        .build()
        .unwrap();

    canvas.set_logical_size(1280, 720);

    let mut event_pump = sdl2_context.event_pump().unwrap();

    let mut state_manager = StateManager::new();

    let mut texture_creator = canvas.texture_creator();
    let mut texture_manager = TextureManager::with(&texture_creator);

    texture_manager
        .load(
            String::from("sprite"),
            Path::new("./data/sprite/protagonist.png"),
        )
        .unwrap();
    texture_manager
        .load(
            String::from("projectile"),
            Path::new("./data/sprite/projectile.png"),
        )
        .unwrap();

    texture_manager
        .load(String::from("enemy"), Path::new("./data/sprite/robot.png"))
        .unwrap();

    texture_manager
        .load(String::from("box"), Path::new("./data/sprite/box.png"))
        .unwrap();

    texture_manager
        .load(
            String::from("floor_tile"),
            Path::new("./data/tile/floor.png"),
        )
        .unwrap();
    texture_manager
        .load(String::from("wall_tile"), Path::new("./data/tile/wall.png"))
        .unwrap();
    texture_manager
        .load(
            String::from("energy_tile"),
            Path::new("./data/tile/energy_field.png"),
        )
        .unwrap();
    texture_manager
        .load(
            String::from("crystal_tile"),
            Path::new("./data/tile/blue_crystal.png"),
        )
        .unwrap();
    texture_manager
        .load(
            String::from("splitter_tile"),
            Path::new("./data/tile/green_crystal.png"),
        )
        .unwrap();

    texture_manager
        .load(
            String::from("selection"),
            Path::new("./data/sprite/selection_arrow.png"),
        )
        .unwrap();

    texture_manager
        .load(
            String::from("menu_logo"),
            Path::new("./data/sprite/menu-logo.png"),
        )
        .unwrap();

    let mut font_manager = FontManager::with(&ttf_context, &texture_creator);

    font_manager
        .load_font(Path::new("./data/font/FreeMono.ttf"), 16)
        .unwrap();

    let mut render_context: RenderContext = RenderContext {
        canvas,
        texture_manager,
        font_manager,
    };

    let mut audio_manager = AudioManager::new();
    audio_manager
        .load(String::from("pew"), Path::new("./data/audio/pew.ogg"))
        .unwrap();
    audio_manager
        .load(
            String::from("ricochet"),
            Path::new("./data/audio/ricochet.ogg"),
        )
        .unwrap();
    audio_manager
        .load(
            String::from("shatter"),
            Path::new("./data/audio/shatter.ogg"),
        )
        .unwrap();
    audio_manager
        .load(
            String::from("robot_death"),
            Path::new("./data/audio/robot_death.ogg"),
        )
        .unwrap();
    audio_manager
        .load(String::from("death"), Path::new("./data/audio/death.ogg"))
        .unwrap();
    audio_manager
        .load(
            String::from("menu_back"),
            Path::new("./data/audio/broumbroum/broumbroum_menu-back.ogg"),
        )
        .unwrap();
    audio_manager
        .load(
            String::from("menu_select"),
            Path::new("./data/audio/broumbroum/broumbroum_menu-select.ogg"),
        )
        .unwrap();
    audio_manager
        .load(
            String::from("menu_change"),
            Path::new("./data/audio/menu-change.wav"),
        )
        .unwrap();

    let mut world: World = World::new();

    world.register::<Position>();
    world.register::<Collideable>();
    world.register::<Collision>();
    world.register::<WallCollision>();
    world.register::<Velocity>();
    world.register::<PlayerControlled>();
    world.register::<Enemy>();
    world.register::<Crate>();
    world.register::<LineOfSight>();
    world.register::<Projectile>();
    world.register::<FireRateLimited>();
    world.register::<Sprite>();

    let mut game_controller_subsystem = sdl2_context.game_controller().unwrap();
    let mut controller = game_controller_subsystem.open(0);

    let input = Input::new();
    world.insert(input);

    world.insert(AudioEvents { list: Vec::new() });

    let mut previous_time = Instant::now();
    world.insert(DeltaTime::new(0, 0));

    world.insert(ElapsedTime(0.0));

    world.insert(MessageQueue(Vec::new()));

    world.insert(CurrentLevelPath(None));

    // Load player save
    println!("Loading player save...");

    let game_save = GameSave::load();

    world.insert(game_save);

    // Load player configs
    println!("Loading player configs...");

    let game_configs = GameConfigs::load();

    world.insert(game_configs);

    println!("Configs loaded.");

    println!("Loading music...");

    let music = sdl2::mixer::Music::from_file("./data/audio/tumocs/miamistarsky-rc2.ogg").unwrap();
    music.play(-1).unwrap();

    println!("Music loaded successfully.");

    let mut slack_time = Duration::new(0, 0);

    state_manager.add_state(&mut world, Box::new(MainMenuState::new()));

    'mainloop: loop {
        let current_delta = previous_time.elapsed();
        previous_time = Instant::now();

        slack_time += current_delta;

        {
            let mut audio = world.write_resource::<AudioEvents>();

            let configs = world.read_resource::<GameConfigs>();

            sdl2::mixer::Music::set_volume(configs.music_volume);

            for sfx in audio.list.iter() {
                if let Some(chunk) = audio_manager.get(&sfx) {
                    Channel::all().set_volume(configs.sfx_volume);
                    Channel::all().play(chunk, 0);
                }
            }

            audio.list.clear();
        }

        {
            let mut message_queue = world.write_resource::<MessageQueue>();

            for message in message_queue.0.drain(0..) {
                match message {
                    GameEngineMessage::ToggleFullscreen => {
                        use sdl2::video::FullscreenType;
                        if render_context.canvas.window().fullscreen_state() == FullscreenType::Off
                        {
                            render_context
                                .canvas
                                .window_mut()
                                .set_fullscreen(FullscreenType::Desktop);
                        } else {
                            render_context
                                .canvas
                                .window_mut()
                                .set_fullscreen(FullscreenType::Off);
                        }
                    }
                }
            }
        }

        while slack_time > Duration::from_millis(17) {
            {
                let mut delta = world.write_resource::<DeltaTime>();

                *delta = Duration::from_millis(17);
            }

            {
                let mut input = world.write_resource::<Input>();

                input.reset();
            }

            for event in event_pump.poll_iter() {
                let mut input = world.write_resource::<Input>();

                match &event {
                    Event::Quit { .. } => break 'mainloop,
                    Event::KeyDown { .. } | Event::KeyUp { .. } => {
                        keyboard_handler(&mut input, &event)
                    }
                    Event::MouseButtonDown { .. }
                    | Event::MouseButtonUp { .. }
                    | Event::MouseMotion { .. } => mouse_handler(&mut input, &event),
                    Event::ControllerButtonDown { .. }
                    | Event::ControllerButtonUp { .. }
                    | Event::ControllerAxisMotion { .. } => gamepad_handler(&mut input, &event),
                    Event::ControllerDeviceAdded { which, .. } => {
                        controller = game_controller_subsystem.open(*which);

                        if let Ok(ref game_controller) = controller {
                            println!("Found game controller: {}", game_controller.name());
                        }
                    }
                    _ => (),
                }
            }

            state_manager.update(&mut world);
            world.maintain();

            slack_time -= Duration::from_millis(17);
        }

        if state_manager.has_quit {
            break 'mainloop;
        }

        render_context.canvas.set_draw_color(Color::RGB(0, 0, 0));
        render_context.canvas.clear();

        state_manager.draw(&mut render_context, &mut world);

        render_context.canvas.present();
    }
}
