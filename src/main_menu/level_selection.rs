use specs::World;
use specs::WorldExt;

use sdl2::pixels::Color;
use sdl2::rect::Rect;

use crate::gameplay::*;
use crate::input::*;
use crate::render::*;
use crate::state::*;
use crate::CurrentLevelPath;
use crate::ElapsedTime;

use crate::gameplay::level::load_level;
use crate::main_menu::MainMenuState;
use crate::AudioEvents;
use crate::GameSave;

use std::path::Path;
use std::path::PathBuf;

fn render_level_selection_menu(
    render_context: &mut RenderContext,
    level_selection_state: &LevelSelectionState,
    save_data: &GameSave,
) {
    let index = level_selection_state.index;

    let y_base_offset = if index > 20 {
        (index as i32 - 20) * 30 * (-1)
    } else {
        0
    };
    let mut y_offset = 50;

    let cursor_position = if index < 20 {
        50 + (index as i32) * 30
    } else {
        50 + 30 * 20
    };

    for path in &level_selection_state.level_dirs {
        let color = if save_data.level_times.contains_key(path) {
            Color::RGB(0, 255, 0)
        } else {
            Color::RGB(0, 100, 0)
        };

        if let Some(texture) = render_context
            .font_manager
            .render(path.file_name().unwrap().to_str().unwrap(), color)
        {
            let width = texture.query().width;
            let height = texture.query().height;

            render_context.canvas.copy(
                &texture,
                None,
                Rect::new(200, y_offset + y_base_offset, width, height),
            );

            y_offset += 30;
        }
    }

    if let Some(texture) = render_context.texture_manager.get("selection") {
        let width = texture.query().width;
        let height = texture.query().height;

        render_context.canvas.copy(
            texture,
            None,
            Rect::new(175, cursor_position, width, height),
        );
    }

    let selected_level = &level_selection_state.level_dirs[level_selection_state.index];

    render_context
        .canvas
        .set_draw_color(Color::RGB(100, 100, 100));
    render_context
        .canvas
        .fill_rect(Rect::new(600, 50, 320, 200));

    if let Some(level_preview) = render_context
        .texture_manager
        .get(selected_level.to_str().unwrap())
    {
        render_context
            .canvas
            .copy(level_preview, None, Rect::new(600, 50, 320, 200));
    } else {
        render_context.texture_manager.load(
            String::from(selected_level.to_str().unwrap()),
            &selected_level.join("preview.png"),
        );
    }

    if let Some(time) = save_data.level_times.get(selected_level) {
        if let Some(texture) = render_context.font_manager.render(
            &format!("Completed: {:.2} seconds", time),
            Color::RGB(255, 255, 255),
        ) {
            let width = texture.query().width;
            let height = texture.query().height;

            render_context
                .canvas
                .copy(&texture, None, Rect::new(600, 300, width, height));
        }
    } else {
        if let Some(texture) = render_context
            .font_manager
            .render(&format!("Not completed"), Color::RGB(255, 255, 255))
        {
            let width = texture.query().width;
            let height = texture.query().height;

            render_context
                .canvas
                .copy(&texture, None, Rect::new(600, 300, width, height));
        }
    }
}

pub struct LevelSelectionState {
    level_dirs: Vec<PathBuf>,
    index: usize,
}

impl LevelSelectionState {
    pub fn new() -> LevelSelectionState {
        let mut level_dirs = Vec::new();

        for entry in Path::new("./data/levels")
            .read_dir()
            .expect("Couldn't open ./data/levels: Directory not found")
        {
            if let Ok(entry) = entry {
                level_dirs.push(entry.path());
            }
        }

        level_dirs.sort();

        LevelSelectionState {
            level_dirs,
            index: 0,
        }
    }
}

impl State for LevelSelectionState {
    fn draw(&mut self, render_context: &mut RenderContext, world: &mut World) {
        let game_save = world.read_resource::<GameSave>();

        render_level_selection_menu(render_context, self, &game_save);
    }

    fn init(&mut self, world: &mut World) {
        let game_save = world.read_resource::<GameSave>();

        self.index = game_save.last_level_index;
    }

    fn update(&mut self, world: &mut World) -> Transition {
        let mut activate = false;
        let mut back = false;

        {
            let input = world.read_resource::<Input>();

            let mut audio_events = world.write_resource::<AudioEvents>();

            if input.key_pressed.down {
                if self.index < self.level_dirs.len() - 1 {
                    self.index += 1;
                }
                audio_events.list.push(String::from("menu_change"));
            } else if input.key_pressed.up {
                if self.index > 0 {
                    self.index -= 1;
                }
                audio_events.list.push(String::from("menu_change"));
            }

            if input.key_pressed.activate {
                activate = true;
                audio_events.list.push(String::from("menu_select"));
            }

            if input.key_pressed.back {
                back = true;
                audio_events.list.push(String::from("menu_back"));
            }
        }

        if back {
            return Transition::Replace(Box::new(MainMenuState::new()));
        }

        if activate {
            {
                let mut game_save = world.write_resource::<GameSave>();
                game_save.last_level_index = self.index;
            }

            world.delete_all();

            match load_level(world, self.level_dirs[self.index].as_path()) {
                Err(e) => println!("Failed to load level: {}", e),
                Ok(()) => {
                    println!(
                        "Level loaded successfully: {}",
                        self.level_dirs[self.index].to_str().unwrap()
                    );
                    let mut current_level_path = world.write_resource::<CurrentLevelPath>();

                    current_level_path.0 = Some(self.level_dirs[self.index].clone());
                }
            }

            let mut timer = world.write_resource::<ElapsedTime>();

            timer.0 = 0.0;

            return Transition::Replace(Box::new(GameplayState::new()));
        }

        Transition::Continue
    }
}
