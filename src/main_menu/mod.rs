use specs::Builder;
use specs::System;
use specs::World;
use specs::WorldExt;

use sdl2::pixels::Color;
use sdl2::rect::Rect;

use crate::gameplay::*;
use crate::input::*;
use crate::render::*;
use crate::state::*;

use crate::component::*;
use crate::gameplay::level::load_level;
use crate::AudioEvents;

use std::path::Path;

pub mod level_selection;
use level_selection::LevelSelectionState;

pub mod options_menu;
use options_menu::OptionsMenuState;

fn render_main_menu(render_context: &mut RenderContext, index: i32) {
    if let Some(text) = render_context
        .font_manager
        .render("PLAY", Color::RGB(255, 255, 255))
    {
        let width = text.query().width;
        let height = text.query().height;

        render_context
            .canvas
            .copy(&text, None, Rect::new(600, 300, width, height));
    }

    if let Some(text) = render_context
        .font_manager
        .render("OPTIONS", Color::RGB(255, 255, 255))
    {
        let width = text.query().width;
        let height = text.query().height;

        render_context
            .canvas
            .copy(&text, None, Rect::new(600, 400, width, height));
    }

    if let Some(text) = render_context
        .font_manager
        .render("QUIT", Color::RGB(255, 255, 255))
    {
        let width = text.query().width;
        let height = text.query().height;

        render_context
            .canvas
            .copy(&text, None, Rect::new(600, 500, width, height));
    }

    if let Some(texture) = render_context.texture_manager.get("selection") {
        let width = texture.query().width;
        let height = texture.query().height;

        render_context.canvas.copy(
            texture,
            None,
            Rect::new(550, 300 + index * 100, width, height),
        );
    }
}

const menu_options: [&str; 3] = ["play", "options", "quit"];

pub const menu_animation_frames: &[Frame] = &[
    frame(300 * 0, 0, 300, 200),
    frame(300 * 1, 0, 300, 200),
    frame(300 * 2, 0, 300, 200),
    frame(300 * 3, 0, 300, 200),
    frame(300 * 3, 0, 300, 200),
    frame(300 * 4, 0, 300, 200),
    frame(300 * 5, 0, 300, 200),
    frame(300 * 6, 0, 300, 200),
    frame(300 * 7, 0, 300, 200),
    frame(300 * 8, 0, 300, 200),
];

pub struct MainMenuState {
    index: usize,
    sprite_renderer: SpriteRenderer,
    animation_system: AnimationSystem,
}

impl MainMenuState {
    pub fn new() -> MainMenuState {
        MainMenuState {
            index: 0,
            sprite_renderer: SpriteRenderer::new(),
            animation_system: AnimationSystem,
        }
    }
}

impl State for MainMenuState {
    fn draw(&mut self, render_context: &mut RenderContext, world: &mut World) {
        render_main_menu(render_context, self.index as i32);

        self.sprite_renderer
            .render(render_context, world.system_data());
    }

    fn init(&mut self, world: &mut World) {
        world.delete_all();

        let mut animation = Animation::new(menu_animation_frames, 0.25);
        animation.animation_transition = Some(menu_animation_frames);

        world
            .create_entity()
            .with(Sprite {
                name: String::from("menu_logo"),
                animation,
                timer: 0.0,
                rotation: 0.0,
                flipped: false,
                effects: SpriteEffects::new(),
            })
            .with(Position { x: 650.0, y: 150.0 })
            .build();
    }

    fn update(&mut self, world: &mut World) -> Transition {
        self.animation_system.run(world.system_data());

        let mut audio_events = world.write_resource::<AudioEvents>();
        let input = world.read_resource::<Input>();

        if input.key_pressed.up {
            if self.index > 0 {
                self.index -= 1;
            }

            audio_events.list.push(String::from("menu_change"));
        }

        if input.key_pressed.down {
            if self.index < menu_options.len() - 1 {
                self.index += 1;
            }

            audio_events.list.push(String::from("menu_change"));
        }

        if input.key_pressed.activate {
            audio_events.list.push(String::from("menu_select"));
            if menu_options[self.index] == "play" {
                return Transition::Replace(Box::new(LevelSelectionState::new()));
            } else if menu_options[self.index] == "options" {
                return Transition::Replace(Box::new(OptionsMenuState::new()));
            } else if menu_options[self.index] == "quit" {
                return Transition::Quit;
            }
        }

        Transition::Continue
    }
}
