use specs::World;
use specs::WorldExt;

use sdl2::pixels::Color;
use sdl2::rect::Rect;

use std::fs::File;
use std::io::Write;

use crate::input::*;
use crate::render::*;
use crate::state::*;
use crate::GameConfigs;

use crate::AudioEvents;

use crate::GameEngineMessage;
use crate::MessageQueue;

use crate::main_menu::MainMenuState;

const menu_options: [&str; 3] = ["sfx", "music", "fullscreen"];

pub struct OptionsMenuState {
    index: usize,
}

impl OptionsMenuState {
    pub fn new() -> OptionsMenuState {
        OptionsMenuState { index: 0 }
    }
}

impl State for OptionsMenuState {
    fn draw(&mut self, render_context: &mut RenderContext, world: &mut World) {
        let configs = world.read_resource::<GameConfigs>();

        if let Some(text) = render_context
            .font_manager
            .render("OPTIONS", Color::RGB(255, 255, 255))
        {
            let width = text.query().width;
            let height = text.query().height;

            render_context
                .canvas
                .copy(&text, None, Rect::new(600, 100, width, height));
        }

        if let Some(text) = render_context.font_manager.render(
            &format!(
                "SFX Volume: {:.0}%",
                (configs.sfx_volume as f32 / 128.0 * 100.0)
            ),
            Color::RGB(255, 255, 255),
        ) {
            let width = text.query().width;
            let height = text.query().height;

            render_context
                .canvas
                .copy(&text, None, Rect::new(600, 200, width, height));
        }

        if let Some(text) = render_context.font_manager.render(
            &format!(
                "Music Volume: {:.0}%",
                (configs.music_volume as f32 / 128.0 * 100.0)
            ),
            Color::RGB(255, 255, 255),
        ) {
            let width = text.query().width;
            let height = text.query().height;

            render_context
                .canvas
                .copy(&text, None, Rect::new(600, 300, width, height));
        }

        if let Some(text) = render_context
            .font_manager
            .render("Toggle fullscreen", Color::RGB(255, 255, 255))
        {
            let width = text.query().width;
            let height = text.query().height;

            render_context
                .canvas
                .copy(&text, None, Rect::new(600, 400, width, height));
        }

        if let Some(texture) = render_context.texture_manager.get("selection") {
            let width = texture.query().width;
            let height = texture.query().height;

            render_context.canvas.copy(
                texture,
                None,
                Rect::new(550, 200 + (self.index as i32) * 100, width, height),
            );
        }
    }

    fn init(&mut self, world: &mut World) {}

    fn update(&mut self, world: &mut World) -> Transition {
        let mut audio_events = world.write_resource::<AudioEvents>();
        let input = world.read_resource::<Input>();

        let mut configs = world.write_resource::<GameConfigs>();
        let mut message_queue = world.write_resource::<MessageQueue>();

        if input.key_pressed.up {
            if self.index > 0 {
                self.index -= 1;
            }

            audio_events.list.push(String::from("menu_change"));
        }

        if input.key_pressed.down {
            if self.index < menu_options.len() - 1 {
                self.index += 1;
            }

            audio_events.list.push(String::from("menu_change"));
        }

        if input.key_pressed.left {
            match menu_options[self.index] {
                "sfx" => {
                    configs.sfx_volume = std::cmp::max(configs.sfx_volume - 5, 0);

                    audio_events.list.push(String::from("menu_change"));
                }
                "music" => {
                    configs.music_volume = std::cmp::max(configs.music_volume - 5, 0);

                    audio_events.list.push(String::from("menu_change"));
                }
                _ => (),
            }
        }

        if input.key_pressed.right {
            match menu_options[self.index] {
                "sfx" => {
                    configs.sfx_volume = std::cmp::min(configs.sfx_volume + 5, 128);

                    audio_events.list.push(String::from("menu_change"));
                }
                "music" => {
                    configs.music_volume = std::cmp::min(configs.music_volume + 5, 128);

                    audio_events.list.push(String::from("menu_change"));
                }
                _ => (),
            }
        }

        if input.key_pressed.activate {
            match menu_options[self.index] {
                "fullscreen" => {
                    message_queue.0.push(GameEngineMessage::ToggleFullscreen);
                }
                _ => (),
            }
        }

        if input.key_pressed.back {
            audio_events.list.push(String::from("menu_back"));

            let mut file = File::create("configs.txt").expect("Couldn't open save file!");
            let string = serde_json::to_string(&*configs).unwrap(); // Yeah... I totally did reference a dereference

            write!(file, "{}", string);

            return Transition::Replace(Box::new(MainMenuState::new()));
        }

        Transition::Continue
    }
}
