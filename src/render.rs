use specs::Join;
use specs::Read;
use specs::ReadStorage;
use specs::System;
use specs::WriteStorage;

use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::rect::Rect;
use sdl2::render::Canvas;
use sdl2::video::Window;

use crate::component::*;
use crate::font_manager::FontManager;
use crate::texture_manager::TextureManager;

use crate::gameplay::level::*;
use crate::input::Input;

use crate::ElapsedTime;

use crate::DeltaTime;

pub struct RenderContext<'s> {
    pub canvas: Canvas<Window>,
    pub texture_manager: TextureManager<'s>,
    pub font_manager: FontManager<'s>,
}

pub struct SpriteRenderer {}

type SpriteSystemData<'a> = (ReadStorage<'a, Sprite>, ReadStorage<'a, Position>);

impl SpriteRenderer {
    pub fn new() -> SpriteRenderer {
        SpriteRenderer {}
    }

    pub fn render(
        &mut self,
        render_context: &mut RenderContext,
        (sprites, positions): SpriteSystemData,
    ) {
        for (sprite, pos) in (&sprites, &positions).join() {
            let mut texture = render_context
                .texture_manager
                .get_mut(&sprite.name)
                .unwrap();

            for effect in sprite.effects.iter() {
                match effect {
                    SpriteEffect::AlphaMod(a) => texture.set_alpha_mod(*a),
                    SpriteEffect::ColourMod(r, g, b) => texture.set_color_mod(*r, *g, *b),
                }
            }

            let src = &sprite.animation.frames[sprite.animation.index];

            render_context
                .canvas
                .copy_ex(
                    texture,
                    src,
                    Rect::from_center((pos.x as i32, pos.y as i32), src.w, src.h),
                    sprite.rotation,
                    None,
                    sprite.flipped,
                    false,
                )
                .unwrap();

            for effect in sprite.effects.iter() {
                match effect {
                    SpriteEffect::AlphaMod(_) => texture.set_alpha_mod(255),
                    SpriteEffect::ColourMod(_, _, _) => texture.set_color_mod(255, 255, 255),
                }
            }
        }
    }
}

pub struct AnimationSystem;

impl<'a> System<'a> for AnimationSystem {
    type SystemData = (Read<'a, DeltaTime>, WriteStorage<'a, Sprite>);

    fn run(&mut self, (delta, mut sprites): Self::SystemData) {
        for sprite in (&mut sprites).join() {
            sprite.timer += delta.as_secs_f32();

            if sprite.timer >= sprite.animation.rate {
                sprite.timer = 0.0;

                if let Direction::Right = sprite.animation.direction {
                    if sprite.animation.index >= sprite.animation.frames.len() - 1 {
                        if let Some(next_animation) = sprite.animation.animation_transition {
                            sprite.animation.index = 0;
                            sprite.animation.frames = next_animation;
                        } else {
                            sprite.animation.direction = Direction::Left;
                        }
                    } else {
                        sprite.animation.index += 1;
                    }
                } else {
                    if sprite.animation.index == 0 {
                        sprite.animation.direction = Direction::Right;
                    } else {
                        sprite.animation.index -= 1;
                    }
                }
            }

            sprite.animation.index %= sprite.animation.frames.len();
        }
    }
}

pub struct LOSRenderer {}

type LOSSystemData<'a> = (ReadStorage<'a, LineOfSight>);

impl LOSRenderer {
    pub fn new() -> LOSRenderer {
        LOSRenderer {}
    }

    pub fn render(&mut self, render_context: &mut RenderContext, (line_of_sights): LOSSystemData) {
        for (los) in (&line_of_sights).join() {
            if (!los.intersects) {
                render_context.canvas.set_draw_color(Color::RGB(255, 0, 0));
            } else {
                render_context.canvas.set_draw_color(Color::RGB(0, 255, 0));
            }

            let (start_x, start_y) = los.start;
            let (target_x, target_y) = los.target;

            render_context.canvas.draw_line(
                (start_x as i32, start_y as i32),
                (target_x as i32, target_y as i32),
            );
        }
    }
}

pub struct LevelRenderer {}

type LevelSystemData<'a> = Read<'a, Level>;

impl LevelRenderer {
    pub fn new() -> LevelRenderer {
        LevelRenderer {}
    }

    pub fn render(&mut self, render_context: &mut RenderContext, level: LevelSystemData) {
        for y in 0..level.height {
            for x in 0..level.width {
                render_context.canvas.set_draw_color(Color::RGB(0, 0, 255));

                match level.tiles[y][x] {
                    Tile::Void => {
                        // Keep background color, no need to redraw
                    }
                    Tile::Floor => {
                        let texture = render_context.texture_manager.get("floor_tile").unwrap();
                        render_context.canvas.copy(
                            texture,
                            None,
                            Rect::new((x as i32) * 35, (y as i32) * 35, 35, 35),
                        );
                    }
                    Tile::Wall => {
                        let texture = render_context.texture_manager.get("wall_tile").unwrap();
                        render_context.canvas.copy(
                            texture,
                            None,
                            Rect::new((x as i32) * 35, (y as i32) * 35, 35, 35),
                        );
                    }
                    Tile::Mirror(hp) => {
                        let texture = render_context.texture_manager.get("crystal_tile").unwrap();
                        render_context.canvas.copy(
                            texture,
                            Rect::new(35 * (4 - hp as i32), 0, 35, 35),
                            Rect::new((x as i32) * 35, (y as i32) * 35, 35, 35),
                        );
                    }
                    Tile::EnergyField => {
                        let texture = render_context.texture_manager.get("energy_tile").unwrap();
                        render_context.canvas.copy(
                            texture,
                            None,
                            Rect::new((x as i32) * 35, (y as i32) * 35, 35, 35),
                        );
                    }
                    Tile::Splitter => {
                        let texture = render_context.texture_manager.get("splitter_tile").unwrap();
                        render_context.canvas.copy(
                            texture,
                            None,
                            Rect::new((x as i32) * 35, (y as i32) * 35, 35, 35),
                        );
                    }
                }
            }
        }
    }
}

pub struct PlayerAimRenderer;

type PlayerAimRendererSystemData<'a> = (
    Read<'a, Level>,
    Read<'a, Input>,
    ReadStorage<'a, PlayerControlled>,
    ReadStorage<'a, Position>,
);

impl PlayerAimRenderer {
    pub fn new() -> PlayerAimRenderer {
        PlayerAimRenderer
    }

    fn cast_ray(level: &Level, (start_x, start_y): (f32, f32), angle: f32) -> (f32, f32) {
        let dx = angle.to_radians().cos();
        let dy = angle.to_radians().sin();

        let mut x = start_x;
        let mut y = start_y;

        'ray: loop {
            x += dx;
            y += dy;

            let tile_x = (x / 35.0) as usize;
            let tile_y = (y / 35.0) as usize;

            if !level.tiles[tile_y][tile_x].is_passable() {
                return (x, y);
            }
        }
    }

    pub fn render(
        &mut self,
        render_context: &mut RenderContext,
        (level, input, players, positions): PlayerAimRendererSystemData,
    ) {
        for (player, position) in (&players, &positions).join() {
            let (rel_x, rel_y) = if input.gamepad_enabled {
                (input.aim_rel_x, input.aim_rel_y)
            } else {
                (
                    input.mouse_x as f32 - position.x,
                    input.mouse_y as f32 - position.y,
                )
            };

            let hypot = rel_x.hypot(rel_y);

            let sin = rel_x / hypot;
            let cos = rel_y / hypot;

            let angle = cos.atan2(sin).to_degrees();

            if !angle.is_nan() {
                let (end_x, end_y) = Self::cast_ray(&level, (position.x, position.y), angle);

                render_context.canvas.set_draw_color(Color::RGB(0, 200, 0));
                render_context.canvas.draw_line(
                    (position.x as i32, position.y as i32),
                    (end_x as i32, end_y as i32),
                );
            }
        }
    }
}

pub struct TimerRenderer;

type TimerRendererSystemData<'a> = (Read<'a, ElapsedTime>);

impl TimerRenderer {
    pub fn new() -> TimerRenderer {
        TimerRenderer
    }

    pub fn render(
        &mut self,
        render_context: &mut RenderContext,
        (elapsed): TimerRendererSystemData,
    ) {
        if let Some(text) = render_context
            .font_manager
            .render(&format!("Time: {:.2}", elapsed.0), Color::WHITE)
        {
            let width = text.query().width;
            let height = text.query().height;

            render_context
                .canvas
                .copy(&text, None, Rect::new(600, 50, width, height));
        }
    }
}
