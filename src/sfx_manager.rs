use std::collections::HashMap;
use std::path::Path;

use sdl2::mixer::Chunk;

pub struct AudioManager {
    sfx_cache: HashMap<String, Chunk>,
}

impl AudioManager {
    pub fn new() -> AudioManager {
        AudioManager {
            sfx_cache: HashMap::new(),
        }
    }

    pub fn get(&self, name: &str) -> Option<&Chunk> {
        self.sfx_cache.get(name)
    }

    pub fn load(&mut self, name: String, path: &Path) -> Result<(), String> {
        let chunk = Chunk::from_file(path)?;

        self.sfx_cache.insert(name, chunk);

        Ok(())
    }

    pub fn destroy(&mut self, name: &str) -> Result<(), String> {
        if self.sfx_cache.contains_key(name) {
            self.sfx_cache.remove(name);

            Ok(())
        } else {
            Err(String::from("No such chunk"))
        }
    }
}

#[derive(Default)]
pub struct AudioEvents {
    pub list: Vec<String>,
}
