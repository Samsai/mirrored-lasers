use sdl2::keyboard::Keycode;
use sdl2::render::Canvas;
use sdl2::video::Window;

use specs::World;

use std::boxed::Box;

use super::RenderContext;

pub enum Transition {
    Pop,
    Push(Box<dyn State>),
    Replace(Box<dyn State>),
    Continue,
    Quit,
}

pub trait State {
    fn draw(&mut self, render_context: &mut RenderContext, world: &mut World);
    fn init(&mut self, world: &mut World);
    fn update(&mut self, world: &mut World) -> Transition;
}

pub struct StateManager {
    state_stack: Vec<Box<dyn State>>,
    pub has_quit: bool,
}

impl StateManager {
    pub fn new() -> StateManager {
        StateManager {
            state_stack: Vec::new(),
            has_quit: false,
        }
    }

    pub fn add_state(&mut self, world: &mut World, mut new_state: Box<dyn State>) {
        new_state.init(world);
        self.state_stack.push(new_state);
    }

    pub fn draw(&mut self, render_context: &mut RenderContext, world: &mut World) {
        for state in self.state_stack.iter_mut() {
            state.draw(render_context, world);
        }
    }

    pub fn update(&mut self, world: &mut World) {
        let mut state_opt = self.state_stack.pop();

        if let Some(mut state) = state_opt {
            let transition = state.update(world);

            match transition {
                Transition::Pop => (),
                Transition::Push(mut new_state) => {
                    new_state.init(world);
                    self.state_stack.push(state);
                    self.state_stack.push(new_state);
                }
                Transition::Replace(mut new_state) => {
                    new_state.init(world);
                    self.state_stack.push(new_state)
                }
                Transition::Continue => self.state_stack.push(state),
                Transition::Quit => self.has_quit = true,
            }
        }
    }
}
