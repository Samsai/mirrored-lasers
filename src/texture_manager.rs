use std::collections::HashMap;
use std::path::Path;

use sdl2::render::Texture;
use sdl2::render::TextureCreator;
use sdl2::video::WindowContext;

use sdl2::image::LoadTexture;

pub struct TextureManager<'s> {
    tc: &'s TextureCreator<WindowContext>,
    texture_cache: HashMap<String, Texture<'s>>,
}

impl<'s> TextureManager<'s> {
    pub fn with(tc: &'s TextureCreator<WindowContext>) -> TextureManager<'s> {
        TextureManager {
            tc,
            texture_cache: HashMap::new(),
        }
    }

    pub fn get(&self, name: &str) -> Option<&Texture> {
        self.texture_cache.get(name)
    }

    pub fn get_mut(&mut self, name: &str) -> Option<&mut Texture<'s>> {
        self.texture_cache.get_mut(name)
    }

    pub fn destroy(&mut self, name: &str) -> Result<(), String> {
        if self.texture_cache.contains_key(name) {
            self.texture_cache.remove(name);

            Ok(())
        } else {
            Err(String::from("No such texture"))
        }
    }

    pub fn load(&mut self, name: String, path: &Path) -> Result<(), String> {
        let texture = self.tc.load_texture(path)?;
        self.texture_cache.insert(name, texture);

        Ok(())
    }
}
